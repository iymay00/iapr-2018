#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import os
import skimage.io
import matplotlib.pyplot as plt
import segmentation_functions as seg
import math

# Set some macros to control plotting
plot_raw = False
plot_normalize = False
plot_shapes_mask = False
plot_digits = False
plot_shapes = False
plot_arrow = True


image_sel = 14

# Directory of the images relative to the current directory
data_base_path = os.path.join(os.pardir, 'data')
data_folder = 'Parcours_updated'
data_path = os.path.join(data_base_path, data_folder)

#print "Loading the images..."

# Load images. Make sure they are in the LAB color space
number_of_images = 32
im_names = [str(i) for i in range(1,number_of_images+1,1)]
filenames = [os.path.join(data_path, name) + '.jpg' for name in im_names]
ic = skimage.io.imread_collection(filenames)
images = skimage.io.concatenate_images(ic)

black_ref = skimage.io.imread(os.path.join(data_path,'black_ref')+'.jpg')

for i in range(6,8):

	# Select an image to send through the pipeline
	image = images[i]
	
	image_equalized = seg.intensity_normalization(image)
	
	brown_mask = seg.remove_brown_background(image_equalized)
	
	# Get the arrow negative
	arrow_negative = seg.get_arrow_negative(image, black_ref, brown_mask)

	# Extract just the arrow on its own.
	arrow = seg.extract_arrow(arrow_negative)
	arrow_image = arrow[0]						# Extract the arrow binary image
	arrow_head_coordinates = arrow[1]			# Extract the arrow head coordinates
	arrow_tail_coordinates = arrow[2]			# Extract the arrow tail coordinates

	# Get the arrow angle
	arrow_angle = seg.get_arrow_angle(image, arrow_head_coordinates, arrow_tail_coordinates)
	
	if plot_arrow:

		fig, ax = plt.subplots(1, 2, figsize=(6, 6))
		ax[0].imshow(arrow_image, cmap='gray')
		ax[0].axis('off')
		ax[0].set_title("Robot Arrow angle:" + str(arrow_angle) + " degrees. Head: " + str(arrow_head_coordinates) + " Tail: " + str(arrow_tail_coordinates))
		ax[1].imshow(image)
		ax[1].axis('off')
#		plt.show()

# Show all plots
plt.show()
