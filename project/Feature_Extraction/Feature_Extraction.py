#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import numpy as np

#%%
###########################################################
########## FEATURE EXTRACTION #############################
###########################################################
import Feature_Extraction_Functions as feat
import os

def classify_images(segments, digits):

    ######### Standardize the images #################
    # get area of each segmented shape
    sums = []
    for i in range(len(segments)):
        sums.append(np.sum(segments[i]))

    ##############################
    # GET NUMBERED SHAPES ########
    ##############################

    # Separate all shapes that have numbers from those that don't
    numbered_indices = feat.get_numbered_shape_index(segments, digits)
    numbered_segs = []
    for i in range(len(numbered_indices)):
        numbered_segs.append( segments[numbered_indices[i]] )


    unnumbered_indices = feat.get_unnumbered_shape_index(segments, digits)
    unnumbered_segs = []
    for i in range(len(unnumbered_indices)):
        unnumbered_segs.append( segments[unnumbered_indices[i]] )

    ###### Identify Home #####
    # create reference circle
    circle_img_size = segments[0].shape
    circle_img = np.zeros(circle_img_size)
    np.sqrt(np.mean(sums)/np.pi)
    circle_radius = np.sqrt(np.mean(sums)/np.pi)
    circle_center = circle_img_size[0]//2
    for i in range(circle_img_size[0]):
        for j in range(circle_img_size[1]):
            if (i-circle_center)**2 + (j-circle_center)**2 <= circle_radius**2 :
                circle_img[i,j] = 1

    numbered_segs.append( circle_img )
    circle_label = 500

    # compare circle image to unnamed objects to find home
    circle_score = 200**2
    for i in range(len(unnumbered_segs)):
        score = np.sum( (unnumbered_segs[i]-circle_img)**2 )
        if score < circle_score:
            circle_score = score
            home_index_unnumbered = i
        
    
    ##################################################
    ########### Calculate Feature Values #############
    ##################################################

    descriptors = range(1,100)

    print("calculating features")

    # calculate freeman code
    #numbered_freeman = feat.freeman_code(numbered_segs)
    #unnumbered_freeman = feat.freeman_code(unnumbered_segs) 

    # extract fourier descriptors
    numbered_f_descriptors = feat.extract_fourier_descriptors(numbered_segs, descriptors) 
    numbered_f_descriptors = np.abs(numbered_f_descriptors)

    unnumbered_f_descriptors = feat.extract_fourier_descriptors(unnumbered_segs, descriptors) 
    unnumbered_f_descriptors = np.abs(unnumbered_f_descriptors)

    # compactness is area/perimeter**2
    # compactness values are b/w 0.04 & 0.07
    numbered_compactness   = feat.calculate_compactness(numbered_segs)
    unnumbered_compactness = feat.calculate_compactness(unnumbered_segs)


    ##########################################################
    ########## CLUSTERING ####################################
    ##########################################################


    ################################# Combination ################################
    # Compute distances
    distance_fd=[]
    distance_com=[]
    for fd_num, com_num in zip(numbered_f_descriptors, numbered_compactness):
        distance_fd_un = []
        distance_com_un = []
    
        for fd_unnum, com_unnum in zip(unnumbered_f_descriptors,unnumbered_compactness):
            distance_fd_un.append( np.sum(  (fd_num - fd_unnum)**2) )
            distance_com_un.append(np.sqrt( (com_num - com_unnum)**2) )
        
        distance_fd.append(distance_fd_un)
        distance_com.append(distance_com_un)

    # Convert to array
    distance_com = np.array(distance_com)
    distance_fd  = np.array(distance_fd)

    # min-max normalization of distances
    distance_com = (distance_com - np.min(distance_com)) / (np.max(distance_com) - np.min(distance_com))
    distance_fd  = (distance_fd  - np.min(distance_fd))  / (np.max(distance_fd)  - np.min(distance_fd))


    # Indices of unnumbered that minimize distance to numbered
    percent_fd = 0.5
    percent_com = 1-percent_fd
    idx = np.argmin(percent_fd*distance_fd + percent_com*distance_com, axis = 1)

    output = []
    for i in range(len(numbered_segs)):
    
        plt.subplot(121)
        plt.imshow(numbered_segs[i])
        
        plt.subplot(122)
        plt.imshow(unnumbered_segs[idx[i]])
        plt.title('Combination')
        plt.show()            
        
        output.append([numbered_indices[i], unnumbered_indices[idx[i]]])

	output = np.array(output)
    
    return output
