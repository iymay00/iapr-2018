#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import skimage
from skimage.morphology import square


#############################################
# Define a function to detect the contour of any given image 
#############################################
def detect_contour(binary_image):
    
	# Median filter the  images to filter out noise and smoothen the image.
	#image = skimage.filters.median(image, square(3))
	
	# Perform binary closing on the image, which is a dilation followed by an erosion operation
	binary_image = skimage.morphology.binary_dilation(binary_image)
	binary_image = skimage.morphology.binary_erosion(binary_image)
	
	# Find the contours of the images. Contours are represented by an array of coordinates (x, y)
	contour = skimage.measure.find_contours(binary_image, 0)[0]
	
	return contour


#############################################
# Define a function to extract the Fourier descriptions for a list of images. 
#############################################

def extract_fourier_descriptors(images, descriptors_to_extract, plot_contours):
    
	# Count the number of images to process
	num = len(images)
	print "Number of shapes = " + str(num)
    
	# Data structures to hold the contours of the numbers and the extracted descriptors.
	contours = []
	print "Number of descriptors to extract: " + str(len(descriptors_to_extract))
	fourier_descriptors = np.empty( (num, len(descriptors_to_extract)), dtype = complex)

	# Compute the fourier transform of each image
	for i in range(0, num): 
	
		# Find the contour of the images
		contour_temp = detect_contour(images[i])
    
		# Create a n x 2 array of the contour coordinates for each image.
		contours.append(contour_temp)
		
		# Convert each coordinate into a single complex number instead of tuples. 
		# Perform a DFT on this array of coordinates represented as a list of complex numbers. 
		dft = np.fft.fft(contour_temp[:,0] + 1j*contour_temp[:,1])
		
		# Extract the desired descriptors as the features of interest. 
		# The descriptors are a series of complex numbers computed by taking the DFT of the original contours.
		descriptors_temp = []
		for descriptor in descriptors_to_extract:
			descriptors_temp.append(dft[descriptor])
		fourier_descriptors[i] = descriptors_temp
   
	if plot_contours:
		# Plot the contours on top of the original images
		fig, axes = plt.subplots(1, num, figsize=(16, 4))
		plt.suptitle('Contour Detection', fontsize=30)
		for ax, im, cont, nm in zip(axes, images, contours, names):
			ax.plot(cont[:,1], cont[:,0], linewidth=3)
			ax.imshow(im, cmap=plt.cm.gray, alpha=1, interpolation='bilinear')
			ax.axis('off')
			ax.set_title(nm)
		plt.show()    
    
	# Return the extracted Fourier descriptors.
	return fourier_descriptors # array where each entry contains the descriptors for the corresponding image

# A function to compute the compacities of all input images
def compute_compacities(shapes):

	# Output a list of compacities
	compacity = []
	
	# Compute compacity for all shapes
	for shape in shapes:

		# Compacity is defined as an image's area over the perimeter squared.
		perimeter = skimage.measure.perimeter(shape)
		area = np.sum(shape)
		compacity.append(float(area/(perimeter**2)))
	
	return compacity
	
# A function to match all shapes.
def match_shapes(shapes):

	plot_contours = False
	
	# The output is a list respecting the order of "shapes." Each element is the index of the matching shape
	matches = np.empty(len(shapes),dtype=int)

	# We wish to extract the first 99 descriptors (excluding the DC component to make them translation invariant)
	descriptors_to_extract = range(1,100)
	
	# Extract the Fourier descriptors for each of the input images. These are a set of complex number for each image
	fourier_descriptors = extract_fourier_descriptors(shapes, descriptors_to_extract, plot_contours)

	# Normalize the Fourier descriptors by the max value.
	print len(fourier_descriptors) # Should be the same length as the number of shapes
	for i in range(0,len(fourier_descriptors)):
	
		# Take the absolute value of the descriptors to make them rotation invariant
		fourier_descriptors[i][:] = np.absolute(fourier_descriptors[i][:])
	
		# Find the normalizing descriptor
		normalizing_descriptor = np.amax(fourier_descriptors[i])
		
		# Normalize the coefficients for the current image. This also makes them scaling invariant.
		fourier_descriptors[i] = fourier_descriptors[i]/normalizing_descriptor
	
	# Compute the compacity for each of the input images
	compacities = compute_compacities(shapes)
	
	# Compute a matching error between a shape and every other shape 
	match_errors = np.empty((len(shapes),len(shapes)))
	fourier_errors = np.empty((len(shapes),len(shapes)))
	compacity_errors = np.empty((len(shapes),len(shapes)))
	
	# Keep track of the min an max errors to implement min-max normalization (we don't just search since there is an infinity error between a shape and itself)
	max_fourier_error = 0
	min_fourier_error = np.inf
	max_compacity_error = 0
	min_compacity_error = np.inf

	for i in range(0,len(shapes)):
		for j in range(0,len(shapes)):
		
			# The error between an image and itself should be inf
			if i == j:
				fourier_errors[i,j] = np.inf
				compacity_errors[i,j] = np.inf
				
			# Otherwise compute the error
			else:
			
				# Compute the raw fourier error as the L2 norm of the difference between the descriptors
				fourier_errors[i,j] = np.sum(np.power(np.subtract(fourier_descriptors[i], fourier_descriptors[j]), 2))
				
				# Update the min and max fourier errors
				if fourier_errors[i,j]  > max_fourier_error:
					max_fourier_error = fourier_errors[i,j] 
					
				if fourier_errors[i,j]  < min_fourier_error:
					min_fourier_error = fourier_errors[i,j] 
				
				# Similarly, compute the raw compacity errors
				compacity_errors[i,j] = np.power(compacities[i] - compacities[j], 2)
				
				# Update the min and max compacity errors
				if compacity_errors[i,j] > max_compacity_error:
					max_compacity_error = compacity_errors[i,j]
					
				if compacity_errors[i,j] < min_compacity_error:
					min_compacity_error = compacity_errors[i,j]
	
	# Perform min/max normalization on the fourier and compacity errors
	fourier_errors = (fourier_errors - min_fourier_error)/(max_fourier_error - min_fourier_error)
	compacity_errors = (compacity_errors - min_compacity_error)/(max_compacity_error - min_compacity_error)
	
	# Compute the match error between every shape and all other shapes
	match_errors = 0.5*(fourier_errors + compacity_errors)
	
	# Finally, the matching shape for any given shape is that which gives the minimum match error
	for i in range(0,len(shapes)):
		matches[i] = np.argmin(match_errors[i])
	
	return matches

