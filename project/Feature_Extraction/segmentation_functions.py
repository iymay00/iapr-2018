#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import os
import sys
import skimage.io
import matplotlib.pyplot as plt
from skimage import exposure
from skimage.feature import canny
from scipy import ndimage as ndi
from skimage.morphology import disk
from skimage.feature import blob_log
from skimage.morphology import square
from matplotlib.patches import Circle, Wedge, Polygon
from skimage.filters import sobel
from skimage.morphology import watershed
from skimage.transform import SimilarityTransform
from skimage.transform import warp
from skimage.morphology import skeletonize
import math
from skimage.color.adapt_rgb import adapt_rgb, each_channel, hsv_value

def snapshot(image, snapshot_enabled):
	if snapshot_enabled:
		fig, ax = plt.subplots(1, 1, figsize=(6, 6))
		ax.imshow(image, cmap='gray')
		ax.axis('off')
		plt.show()
		
		plt.show()
		
		
# Adapting skimage functions to work on multiple channels	
@adapt_rgb(each_channel)
def median_filter_each(image, s):
	st_elem = s
	return  skimage.filters.median(image, selem=square(st_elem))
		
		
# Adapting skimage functions to work on multiple channels	
@adapt_rgb(each_channel)
def equalize_adapthist_each(image):
#	return skimage.exposure.equalize_adapthist(image)
	clip_lim = 0.1	# Maximize the contrast
	k_size = image.shape[0]/10.0
	return skimage.exposure.equalize_adapthist(image, clip_limit = clip_lim, kernel_size=k_size)

@adapt_rgb(hsv_value)
def equalize_adapthist_hsv(image):
	return skimage.exposure.equalize_adapthist(image)

@adapt_rgb(each_channel)
def equalize_each(image):
    return skimage.exposure.equalize_hist(image)

@adapt_rgb(hsv_value)
def equalize_hsv(image):
    return skimage.exposure.equalize_hist(image)
	
@adapt_rgb(each_channel)
def contrast_stretch_each(image, p_low, p_up):
	
	# Contrast stretching.
	p_lower_ch1, p_upper_ch1 = np.percentile([np.iinfo(image.dtype).min, np.iinfo(image.dtype).max], (p_low, p_up))
	#p_lower_ch1, p_upper_ch1 = np.percentile([np.min(image), np.max(image)], (p_low, p_up))
    
	# Rescale the image intensity for each channel
	return exposure.rescale_intensity(image, in_range=(p_lower_ch1, p_upper_ch1))
    

@adapt_rgb(hsv_value)
def contrast_stretch_hsv(image):
	
	# Specify the upper and lower percentile levels and clip limit
	p_up = 100	# Higher value means more dark allowed in the final image
	p_low = 8	# Lower value means more light allowed in the final image

	# Contrast stretching.
	p_lower_ch1, p_upper_ch1 = np.percentile(image, (p_low, p_up))
    
	# Rescale the image intensity for each channel
	return exposure.rescale_intensity(image, in_range=(p_lower_ch1, p_upper_ch1))
		
		
# A function to crop and pad the images
def crop_and_pad(image, x_coordinates, y_coordinates, radii):

	cropped_shapes = []
	final_shapes = []

	for x, y, r in zip(x_coordinates, y_coordinates, radii):
		
		# Compte the dimensions of a square surrounding each image
		r = 4 * r
		
		# Compute the boundaries of the crop
		y_upper_crop= y - (r/2)
		y_lower_crop= image.shape[0] - (y + (r/2))
		x_left_crop= x - (r/2)
		x_right_crop= image.shape[1] - (x + (r/2))

		# Make sure we haven't exceeded the boundaries of the image itself		
		if y_upper_crop < 0:
			y_upper_crop = 0
		if y_lower_crop < 0:
			y_lower_crop = 0 
		if x_left_crop < 0:
			x_left_crop = 0
		if x_right_crop < 0:
			x_right_crop = 0
		
		# Crop out the shape
		cropped_image = skimage.util.crop(image, ((y_upper_crop, y_lower_crop), (x_left_crop,  x_right_crop)) , copy=True)
		cropped_shapes.append(cropped_image)

	# Search for the maximum possible axis length throughout all the crops.
	max_axis = 0
	for c in range(0, len(cropped_shapes) - 1):
		max_axis = max([len(cropped_shapes[c][:,1]), len(cropped_shapes[c][1,:]), max_axis])
	
	# Make sure all the images are the same size. 
	for c in range(0, len(cropped_shapes)):
		
		# If any one of the two image axes are less than the max, pad that axis. 
		final_shape = cropped_shapes[c]
		
		if len(final_shape[:,1]) != len(final_shape[1,:]):
			max_axis = max([len(final_shape[1,:]), len(final_shape[:,1])])
			final_shape = skimage.util.pad(final_shape, ((max_axis - len(final_shape[:,1]), 0), (max_axis - len(final_shape[1,:]), 0)), mode = "constant")

		# Save the final shape
		final_shapes.append(final_shape)	
	
	return final_shapes
	
	
# A function to perform intensity normalization on an image.
def get_arrow_negative(image, black_ref, brown_mask):
    
	snapshot_enabled = False

	# Determine the cutoff of the threshold based on the amount of light present in the image.
	brightness = np.zeros_like(image)
	for i in range(0, image.shape[0]):
		for j in range (0, image.shape[1]):
			brightness[i,j] = sum([image[i,j,0],image[i,j,1],image[i,j,2]])/3.0 ##0 is dark (black) and 255 is bright (white)
			
	# Obtain brightness metrics to allow the contrast stretching and black pixel filtering to adapt to the image's light conditions
	mean_brightness = np.mean(brightness)
	max_brightness = np.amax(brightness)
	min_brightness = np.amin(brightness)
	scaled_std = (1.4*brightness.std())/max_brightness
	scaled_boundery = (mean_brightness - 1.5*brightness.std())/max_brightness
	
	# Compute the scaler for the black pixel threshold
	scaling_constant = mean_brightness/max_brightness
	
	# Perform contrast stretching specifically tailored to the arrow properties.
	p_up = scaled_boundery * 100 #60 #50 # Higher value means more dark allowed in the final image 
	p_low = (scaled_boundery - scaled_std) * 100 #40 #20 (maybe 30) # Lower value means more light allowed in the final image

	image = contrast_stretch_each(image, p_low, p_up)
	image = equalize_adapthist_each(image)

	snapshot(image, snapshot_enabled)
	
	# Remove as many pixels from the black arrow as possible. Set all points above a threshold to zero
	arrow_negative = skimage.color.deltaE_ciede94(image, black_ref)
	
	# Generate the arrow negative
	arrow_negative = arrow_negative > (np.amin(arrow_negative) + scaling_constant*arrow_negative.std())
	snapshot(arrow_negative, snapshot_enabled)
	
    # Remove brown background
	arrow_negative = arrow_negative + (brown_mask == 0)

	snapshot(arrow_negative, snapshot_enabled)

	# Filter out any non-arrow pixels. 
	arrow_negative = skimage.morphology.binary_dilation(arrow_negative, square(10))
	
	# Widen out the space of the arrow
	arrow_negative = skimage.morphology.binary_erosion(arrow_negative, square(15))
	
	snapshot(arrow_negative, snapshot_enabled)
	return arrow_negative

def remove_brown_background(image_equalized):
	#Need to pass the image equalized! Important!
	
	snapshot_enabled = False
	
	# Convert to gray scale
	image_equalized = skimage.color.rgb2gray(image_equalized)
	
	# Get the y indexes of the table pixels (white)
	idx_white = np.nonzero(image_equalized>0.9)[1]
	
	# Create brown mask
	mask_brown = np.ones_like(image_equalized)
	
	# Create left band, from the leftmost pixel until the first one in the top row
	mask_brown[:,:idx_white[0]] = 0
	# Create left band, from the rightmost white pixel until the end
	mask_brown[:,idx_white[-1]+1:] = 0
	
	snapshot(mask_brown, snapshot_enabled)
	
	
	return mask_brown    	
	
	
# A function to determine if a given pixel is directly connected to an intersection pixel. Recursive function.
def number_of_connected_intersections(binary_image, coordinates, intersections, visited_pixels):

	x_coordinate = coordinates[0]
	y_coordinate = coordinates[1]
	
	# Mark the current pixel as having been visted
	visited_pixels[x_coordinate, y_coordinate] = 1
	
	if(binary_image[x_coordinate, y_coordinate] == 0):
		
		# Base case: end of skeleton path
		intersections = intersections + 0
	
	
	else:
	
		# Determine how many pixels are connected to the current pixel
		connections = 0
		connections = (connections + binary_image[x_coordinate+1, y_coordinate] 
									+ binary_image[x_coordinate, y_coordinate+1] 
									+ binary_image[x_coordinate+1, y_coordinate+1] 
									+ binary_image[x_coordinate-1, y_coordinate]
									+ binary_image[x_coordinate, y_coordinate-1]
									+ binary_image[x_coordinate-1, y_coordinate-1]
									+ binary_image[x_coordinate+1, y_coordinate-1]
									+ binary_image[x_coordinate-1, y_coordinate+1])
	
		# Check if the current pixel is an intersection
		if connections > 2:
			intersections = intersections + 1
	
		# Repeat for all connected pixels that have not been previously visited
		if visited_pixels[x_coordinate+1, y_coordinate] == 0:
			intersections = intersections + number_of_connected_intersections(binary_image, [x_coordinate+1, y_coordinate], intersections, visited_pixels)
		
		if visited_pixels[x_coordinate+1, y_coordinate+1] == 0:
			intersections = intersections + number_of_connected_intersections(binary_image, [x_coordinate+1, y_coordinate+1], intersections, visited_pixels)		
		
		if visited_pixels[x_coordinate, y_coordinate+1] == 0:
			intersections = intersections + number_of_connected_intersections(binary_image, [x_coordinate, y_coordinate+1], intersections, visited_pixels)
		
		if visited_pixels[x_coordinate-1, y_coordinate] == 0:
			intersections = intersections + number_of_connected_intersections(binary_image, [x_coordinate-1, y_coordinate], intersections, visited_pixels)
		
		if visited_pixels[x_coordinate-1, y_coordinate-1] == 0:
			intersections = intersections + number_of_connected_intersections(binary_image, [x_coordinate-1, y_coordinate-1], intersections, visited_pixels)
		
		if visited_pixels[x_coordinate, y_coordinate-1] == 0:
			intersections = intersections + number_of_connected_intersections(binary_image, [x_coordinate, y_coordinate-1], intersections, visited_pixels)
		
		if visited_pixels[x_coordinate+1, y_coordinate-1] == 0:
			intersections = intersections + number_of_connected_intersections(binary_image, [x_coordinate+1, y_coordinate-1], intersections, visited_pixels)
		
		if visited_pixels[x_coordinate-1, y_coordinate+1] == 0:
			intersections = intersections + number_of_connected_intersections(binary_image, [x_coordinate-1, y_coordinate+1], intersections, visited_pixels)
	
	return intersections
	
	
# A function to extract the arrow of the robot
def extract_arrow(arrow_negative):

	snapshot_enabled = False
	
	image = (arrow_negative==0)	
	
	snapshot(image, snapshot_enabled)
	image = skimage.morphology.binary_erosion(image, selem=disk(13))	
	snapshot(image, snapshot_enabled)

	# perform skeletonization. The skeleton for the arrow will have 3 vertices, and 1 intersection point.
	image = skeletonize(image)
	
	# Find the coordinates of all skeleton pixels
	skeleton_pixel_coordinates = np.nonzero(image)
	
	# Filter out any pixels that are not directly connected to an intersection pixel.
	for pixel in range(0, len(skeleton_pixel_coordinates[0])):
		intersections = 0
		coordinates = [skeleton_pixel_coordinates[0][pixel], skeleton_pixel_coordinates[1][pixel]]
		visited_pixels = np.zeros_like(image)
		if(number_of_connected_intersections(image, coordinates, intersections, visited_pixels) == 0):
			image[skeleton_pixel_coordinates[0][pixel], skeleton_pixel_coordinates[1][pixel]] = 0
			
	# Find the intersection point and vertices
	intersections = []
	vertices = []
	for i in range(0, image.shape[0]):
	
		for j in range(0, image.shape[1]):
		
			if image[i,j] == True:
			
				# If the pixel has more than 2 neighbors, it is an intersection
				if [image[i+1,j], image[i,j+1], image[i+1,j+1], image[i,j-1], image[i-1,j], image[i-1,j-1], image[i-1,j+1], image[i+1,j-1]].count(True) > 2:
					intersections.append([i,j])
				
				# If the pixel only has 1 neighbor, it is a vertex
				if [image[i+1,j], image[i,j+1], image[i+1,j+1], image[i,j-1], image[i-1,j], image[i-1,j-1], image[i-1,j+1], image[i+1,j-1]].count(True) == 1:
					vertices.append([i,j])

	# Specify the arrow head. It is defined as the "triple point" that is closest to the mean of two vertices.
	min_midpoint_closeness = 1000000
	vertices_means = 0
	max_vertix_distance = 0
	max_arrow_head_metric = 0							# Vertex separation - difference between intersection point and vertix mean
	for intersection in intersections:
	
		# Find the intersection such that it is closest to the midpoint between the two farthest apart vertices
		for i in range(0,len(vertices)-1):
		
			for j in range(i+1, len(vertices)):
				v_1 = vertices[i]
				v_2 = vertices[j]
				vertices_mean = 0.5 * np.add(v_1, v_2)											# Compute the midpoint
				midpoint_closeness = np.linalg.norm(np.subtract(intersection, vertices_mean))	# Euclidian distance between the mean and the intersection point
				vertix_separation = np.linalg.norm(np.subtract(v_1,v_2))						# Euclidian distance between the two vertices
				
				# A metric to quantify how likely the current intersection is a head. Emphasize the importance of one part or the other by introducing scaling constants
				midpoint_gain = 2.0
				separation_gain = 1.0
				arrow_head_metric = (separation_gain*vertix_separation) - (midpoint_gain*midpoint_closeness) 
				
				#arrow_head_metric = separation_gain*(vertix_separation) + (midpoint_gain/(1 + midpoint_closeness))
				#print "V" + str(vertix_separation)
				#print "M" + str(midpoint_closeness)
				#print "A" + str(arrow_head_metric)
				
				# Update the arrow head coordinates if a better candidate is found
				if arrow_head_metric > max_arrow_head_metric:
					max_arrow_head_metric = arrow_head_metric
					arrow_head_coordinates = intersection
					
		
	# The arrow tail is the vertex farthest from the head, ie: the tail point which maximizes the arrow length
	max_arrow_length = 0
	for vertix in vertices:
		v_x = vertix[0]
		v_y = vertix[1]
		arrow_length = ((arrow_head_coordinates[0] - v_x) ** 2) + ((arrow_head_coordinates[1] - v_y) ** 2)	# Euclidian distance
		if arrow_length > max_arrow_length:
			max_arrow_length = arrow_length
			arrow_tail_coordinates = vertix

	# Return a list containing the arrow mask, and the coordinates of the arrow's head and tail.
	return [image, arrow_head_coordinates, arrow_tail_coordinates]
	
	
def get_arrow_angle(image, arrow_head_coordinates, arrow_tail_coordinates):

	# Compute the arrow angle, counterclockwise with respect to the positive real axis		
	head_x = arrow_head_coordinates[1]
	head_y = image.shape[0] - arrow_head_coordinates[0]
	
	tail_x = arrow_tail_coordinates[1]
	tail_y = image.shape[0] - arrow_tail_coordinates[0]
			
	# Compute the angle 		
	arrow_angle = np.arctan(abs(float(head_y - tail_y))/abs(float(head_x - tail_x))) *(180.0/math.pi)
	
	# Correct the angle if it is in the second quadrant
	if (head_x - tail_x < 0)  and (head_y - tail_y > 0):
		arrow_angle = 180 - arrow_angle	

	# Correct the angle if it is in the third quadrant
	if (head_x - tail_x < 0) and (head_y - tail_y < 0):
		arrow_angle = arrow_angle - 180
		
	# Correct the angle if it is in the fourth quadrant
	if (head_x - tail_x > 0) and (head_y - tail_y < 0):
		arrow_angle = -1*arrow_angle
	
	return arrow_angle
	

# A function to perform intensity normalization on an image.
def intensity_normalization(image):
    
	snapshot_enabled = False
	
	# Intensity equalization is done for each channel in the images.
	image_equalized = np.empty(image.shape)
	
	# Specify the upper and lower percentile levels and clip limit
	p_up = 45#40	# Higher value means more dark allowed in the final image 
	p_low = 30#20	# Lower value means more light allowed in the final image
	image_equalized = contrast_stretch_each(image, p_low, p_up)
	
	
	image_equalized = equalize_adapthist_each(image_equalized)
	
	snapshot(image_equalized, snapshot_enabled)
	
	return image_equalized

# A function to perform segmentation on an image
def segment_image(image, arrow_mask, brown_mask):

	snapshot_enabled = False
	
	image = median_filter_each(image, 5)

	# Convert the original image to grayscale
	image = skimage.color.rgb2gray(image) 
	
	# High pass filter to enhance the contours. This is particularly important for the yellow shapes.
	kernel_HPF = np.array([[1, -2, 1],
					[-2,  8, -2],
					[1, -2, 1]])				
	edges = ndi.convolve(image, kernel_HPF)

  	# Use the Canny edge detector in order to identify the edges of the shapes.
    # Reduce the standard deviation in order to INCREASE the edge detector's sensitivity.
	edges = canny(edges, sigma=1)

	# Perform dilation to improve the integrity of the segmented borders.
	shapes_mask = skimage.morphology.binary_dilation(edges, square(5))
	
	# Remove any pixels identified as part of the arrow and the brown floor
	shapes_mask = shapes_mask * arrow_mask
	shapes_mask = shapes_mask * brown_mask 
	
	# Fill the holes that remain within the image using mathematical morphology.
	shapes_mask = ndi.binary_fill_holes(shapes_mask)
	shapes_mask = shapes_mask * arrow_mask

	# Perform dilation to improve the integrity of the segmented borders.
	shapes_mask = skimage.morphology.binary_erosion(shapes_mask, square(30))	
	
	# Now perform region growing with these seeds to get the exact images.
	markers = np.zeros_like(image)
	threshold = skimage.filters.threshold_otsu(image)
	markers[image > (np.amax(image) - 0.1*image.std())] = 1
	markers[(image * shapes_mask) > 0] = 2
	
	# Generate an elevation map of the brain image using the sobel filter.
	elevation_map = sobel(image)#, digits_mask) 
	
	# Perform the segmentation on the elevation map, using the computed markers as starting points for the watershed algorithm.
	shapes_mask = watershed(elevation_map, markers) - 1 # subtract 1 since the outputs are 1 & 2 for foreground and background
	
	# Binary morphology to finalize the shapes
	shapes_mask = skimage.morphology.binary_dilation(shapes_mask, square(5))
	shapes_mask = ndi.binary_fill_holes(shapes_mask)
	shapes_mask = skimage.morphology.binary_erosion(shapes_mask, square(5))
	snapshot(shapes_mask,snapshot_enabled)
	
	return shapes_mask


# A function to extract the image's shapes
def extract_shapes(image, shapes_mask):
	
	plot_blobs = False
	plot_crops = False
	cropped_shapes = []
	final_shapes = []
	
	# Count the number of blobs in the shapes mask
	# We raise the minimum standard deviation for the Gaussian Kernel in order to avoid over-counting blobs.
	blobs = blob_log(shapes_mask, min_sigma=45, max_sigma=70, num_sigma = 5, overlap=0.2)
        	
	# Get all the coordinates of the shapes to extract.
	x_coordinates = [int(blob[1]) for blob in blobs]
	y_coordinates = [int(blob[0]) for blob in blobs]
	radii = [int(blob[2]) for blob in blobs]

	if plot_blobs:
		fig, ax = plt.subplots(1, 1, figsize=(6, 6))
		ax.imshow(shapes_mask)
		ax.axis('off')
		plt.scatter(x_coordinates, y_coordinates)
		
	for x, y, r in zip(x_coordinates, y_coordinates, radii):
		
		# Compute the dimensions of a square surrounding each image
		r = 4 * r

		if plot_blobs:
			crop = plt.Rectangle((x - (r/2), y - (r/2)), r, r, color='r', fill = False)
			ax.add_artist(crop)
		
		# Compute the boundaries of the crop
		y_upper_crop= y - (r/2)
		y_lower_crop= image.shape[0] - (y + (r/2))
		x_left_crop= x - (r/2)
		x_right_crop= image.shape[1] - (x + (r/2))

		# Make sure we haven't exceeded the boundaries of the image itself		
		if y_upper_crop < 0:
			y_upper_crop = 0
		if y_lower_crop < 0:
			y_lower_crop = 0 # image.shape[0]
		if x_left_crop < 0:
			x_left_crop = 0
		if x_right_crop < 0:
			x_right_crop = 0 # image.shape[1]
		
		# Crop out the shape
		cropped_image = skimage.util.crop(shapes_mask, ((y_upper_crop, y_lower_crop), (x_left_crop,  x_right_crop)) , copy=True)
		cropped_shapes.append(cropped_image)

	# Search for the maximum possible axis length throughout all the crops.
	max_axis = 0
	for c in range(0, len(cropped_shapes) - 1):
		max_axis = max([len(cropped_shapes[c][:,1]), len(cropped_shapes[c][1,:]), max_axis])

	# Make sure all the images are the same size. 
	for c in range(0, len(cropped_shapes)):
		
		# If any one of the two image axes are less than the max, pad that axis. Then translate it by the same amount to center it.
		final_shape = cropped_shapes[c]

		if len(final_shape[:,1]) < max_axis:
			
			# Pad the crop
			pad_amount = max_axis - len(final_shape[:,1])
			final_shape = skimage.util.pad(final_shape, ((pad_amount, 0), (0,0)), mode = "constant")

		if len(final_shape[1,:]) < max_axis:
		
			# Pad the crop
			pad_amount = max_axis - len(final_shape[1,:])
			final_shape = skimage.util.pad(final_shape, ((0, 0), (pad_amount,0)), mode = "constant")

		# Save the final shape
		final_shapes.append(final_shape)

	# Print the cropped and padded shapes.
	if plot_crops:
		for c in range(0, len(final_shapes)):
	
			fig, ax = plt.subplots(1, 1, figsize=(6, 6))
			ax.imshow(final_shapes[c], cmap='gray')
			ax.axis('off')
			ax.set_title("A Crop of Dimensions: " + str(len(final_shapes[c][:,1])) + " x " + str(len(final_shapes[c][1,:])))
	plt.show()
	
	# Return a list containing the cropped shapes, and the coordinates of each shape's center in the original image.
	return [final_shapes, x_coordinates, y_coordinates, radii]
	

# A function to extract the image's numbers
def extract_numbers(image, shape_extraction, shapes_mask):

	plot_crops = False
	plot_hist = False
	snapshot_enabled = False
	
    # Equalization for digits
	# Specify the upper and lower percentile levels and clip limit
	p_up = 45	# Higher value means more dark allowed in the final image 
	p_low = 15	# Lower value means more light allowed in the final image
	image = contrast_stretch_each(image, p_low, p_up)
	image = equalize_adapthist_each(image)
	
	# Convert the original image to grayscale
	image = skimage.color.rgb2gray(image)
	
	image = (image > (np.amax(image) - 0.1*image.std())) * shapes_mask	
	
	# Binary morphology to remove small "salt" noise
	image = skimage.morphology.binary_opening(image, square(3))
	
	snapshot(image, snapshot_enabled)
	
	# Extract the shapes as defined by the shape extraction parameters	
	x_coordinates = shape_extraction[1]
	y_coordinates = shape_extraction[2]
	radii = shape_extraction[3]
	
	final_shapes = crop_and_pad(image, x_coordinates, y_coordinates, radii)

	# Print the cropped and padded shapes.
	if plot_crops:
		for c in range(0, len(final_shapes)):
	
			fig, ax = plt.subplots(1, 1, figsize=(6, 6))
			ax.imshow(final_shapes[c], cmap='gray')
			ax.axis('off')
			ax.set_title("A Crop of Dimensions: " + str(len(final_shapes[c][:,1])) + " x " + str(len(final_shapes[c][1,:])))
	
	plt.show()	
	
	# Return a list containing the cropped shapes, and the coordinates of each shape's center in the original image.
	return [final_shapes, x_coordinates, y_coordinates, radii]


	
	
	
	

