#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import os
import skimage.io
import matplotlib.pyplot as plt
import segmentation_functions as seg

# Set some macros to control plotting
plot_raw = False
plot_normalize = False
plot_shapes_mask = False
plot_digits = False
plot_shapes = False


image_sel = 0

# Directory of the images relative to the current directory
data_base_path = os.path.join(os.pardir, 'data')
data_folder = 'Parcours_updated'
data_path = os.path.join(data_base_path, data_folder)

#print "Loading the images..."

# Load images. Make sure they are in the LAB color space
number_of_images = 24
im_names = [str(i) for i in range(1,number_of_images+1,1)]
filenames = [os.path.join(data_path, name) + '.jpg' for name in im_names]
ic = skimage.io.imread_collection(filenames)
images = skimage.io.concatenate_images(ic)

# Select an image to send through the pipleline
image = images[image_sel]

# Plot images, select whether to plot
if plot_raw:
	fig, ax = plt.subplots(1, 1, figsize=(6, 6))
	ax.imshow(image)
	ax.axis('off')


# Normalize the intensities of the images.
#print "Normalizing the image intensity..."
image_equalized = seg.intensity_normalization(image)
if plot_normalize:
	fig, ax = plt.subplots(1, 1, figsize=(6, 6))
	ax.imshow(image_equalized)
	ax.axis('off')


# Segment the image shapes (excluding the black arrow)
#print "Performing segmentation..."
shapes_mask = seg.segment_image(image_equalized)
if plot_shapes_mask:
	fig, axes = plt.subplots(1, 2, figsize=(12, 12))
	for ax, im in zip(axes.ravel(), [images[image_sel], shapes_mask]):
		ax.imshow(im)
		ax.axis('off')

#print "Extracting shapes..."
shapes_extraction = seg.extract_shapes(image_equalized, shapes_mask)
shapes = shapes_extraction[0]
if plot_shapes:
	for c in range(0, len(shapes)):
		fig, ax = plt.subplots(1, 1, figsize=(6, 6))
		ax.imshow(shapes[c], cmap='gray')
		ax.axis('off')
		ax.set_title("A Crop of Dimensions: " + str(len(shapes[c][:,1])) + " x " + str(len(shapes[c][1,:])))

import pickle
with open('shapes.pkl','wb') as f:
    pickle.dump(shapes, f)

#print "Extracting digits..."
digits_extraction = seg.extract_numbers(image, shapes_extraction, shapes_mask)
digits = digits_extraction[0]
if plot_digits:
	for c in range(0, len(digits)):
		fig, ax = plt.subplots(1, 1, figsize=(6, 6))
		ax.imshow(digits[c], cmap='gray')
		ax.axis('off')
		ax.set_title("A Crop of Dimensions: " + str(len(digits[c][:,1])) + " x " + str(len(digits[c][1,:])))

# Show all plots
plt.show()
