#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import os
import skimage.io
import matplotlib.pyplot as plt
import math
import torch
from skimage.transform import resize
from iapr.webcam import WebcamVideoStream
from iapr.robot import Robot
import Segmentation.segmentation_functions as seg
from Digits_classification import *
from Digits_classification.model_cnn_complex import *
from Digits_classification.classify_digits import *
import controller_functions as control
import matching_functions as m

# Create a webcam video stream
#   Note: src=0 links to the USB Webcam of the computers provided for the project
wvs = WebcamVideoStream(src=0)

# Read most recent frame
image = wvs.read()

# Create Robot instance
robot = Robot(hostname='ev3dev.local')

# Directory of the images relative to the current directory
data_base_path = os.path.join('data')
data_folder = 'Parcours_updated'
data_path = os.path.join(data_base_path, data_folder)
black_ref = skimage.io.imread(os.path.join(data_path,'black_ref')+'.jpg')

# Normalize the intensities of the images.
image_equalized = seg.intensity_normalization(image)

# Extract a brown mask to filter out the floor from the image
brown_mask = seg.remove_brown_background(image_equalized)

# Get the arrow mask which will later be used to filter out the arrow from the shapes segmentation	
arrow_mask = seg.get_arrow_negative(image, black_ref, brown_mask)

# Segment the image shapes (excluding the black arrow)
shapes_mask = seg.segment_image(image_equalized, arrow_mask, brown_mask)

#plt.imshow(shapes_mask)
#plt.show()

# Extract the shapes, and each shape's corresponding coordinates
shapes_extraction = seg.extract_shapes(image_equalized, shapes_mask)
shapes = shapes_extraction[0]
shapes_y_coordinates = shapes_extraction[1]
shapes_x_coordinates = shapes_extraction[2]

# Extract the digits, and each digits' corresponding coordinates
digits_extraction = seg.extract_numbers(image, shapes_extraction, shapes_mask)
digits_y_coordinates = digits_extraction[1]
digits_x_coordinates = digits_extraction[2]
digits = digits_extraction[0]

# Add a circle to the shapes structure. The image that matches this will be the home shape that we need to go to at the end of the course.
circle_img_size = shapes[0].shape
circle_img = np.zeros(circle_img_size)
np.sqrt(np.mean(sums)/np.pi)
circle_radius = np.sqrt(np.mean(sums)/np.pi)
circle_center = circle_img_size[0]//2
for i in range(circle_img_size[0]):

	for j in range(circle_img_size[1]):
		if (i-circle_center)**2 + (j-circle_center)**2 <= circle_radius**2 :
			circle_img[i,j] = 1
			
# Append the circle to the shapes structure. The last element of shapes_match_map holds the index of shapes corresponding to the home circle
shapes.append(circle_img)

# For every shape, find its match
shape_match_map = m.match_shapes(shapes)

# Determine destinations of first the number, then its matching shape
model = torch.load('Digits_classification/model_cnn_complex.pt')
digits_class=obtain_prediction(model, digits, False)

# Sort the digits to determine the path the robot should follow
sorted_digits=np.argsort(digits_class)

#destination = [digits_x_coordinates[sorted_digits[number_empty]], digits_y_coordinates[sorted_digits[number_empty]]]
#match_destination = [shapes_x_coordinates[shape_match_map[sorted_digits[number_empty]]], shapes_y_coordinates[shape_match_map[sorted_digits[number_empty]]]]
#print(sorted_digits[number_empty])
#print(sorted_digits)
#print(digits_class[sorted_digits[number_empty]])
#print(digits_class)
#sorted_digits
#plt.imshow(digits[sorted_digits[number_empty]])	
#plt.show()			


# Begin the course
working = True
angle_threshold = 20
distance_threshold = 100
	
# Navigate to each number, and then its pairing shape
number_empty=np.sum(np.array(digits_class)==-1)					# The number of shapes with no digits printed on them
for i in range(number_empty, len(digits_class)):
	
	# Determine the two destinations for this loop. Destination is the number, and match_destination is its matching shape
	destination = [digits_x_coordinates[sorted_digits[i]], digits_y_coordinates[sorted_digits[i]]]
	match_destination = [shapes_x_coordinates[shape_match_map[sorted_digits[i]]], shapes_y_coordinates[shapes_match_map[sorted_digits[i]]]]
	
	# Go to the number
	working = True
	while working:

		# Read most recent frame
		image = wvs.read()
		
		# Get the arrow mask which will later be used to filter out the arrow from the shapes segmentation	
		arrow_mask = seg.get_arrow_negative(image, black_ref, brown_mask)
		
		# Extract just the arrow on its own.
		arrow = seg.extract_arrow(arrow_mask)
		arrow_image = arrow[0]						# Extract the arrow binary image
		arrow_head_coordinates = arrow[1]			# Extract the arrow head coordinates
		arrow_tail_coordinates = arrow[2]			# Extract the arrow tail coordinates
		
		#  Compute how far the robot is from the destination, and how much rotation is needed.
		angle = control.compute_rotation(image_equalized, arrow_head_coordinates, arrow_tail_coordinates, destination)
		distance = control.compute_displacement(arrow_head_coordinates, destination)
		
#		print('Distance: {}'.format(distance))

		# If the robot angle is offset more than the threshold, rotate the robot
		if abs(angle) > angle_threshold:
			robot.steer_in_place(angle)

		# If the robot is still too far away from the destination, then move the robot forward
		if distance > distance_threshold:
			robot.move_forward(30)
	
		# If we are close enough to the robot, stop moving and prepare to go to the new destination (The matching shape)
		if distance <= distance_threshold: 
			working = False
			
			# Beep once to indicate that we have successfully picked up a piece
			robot.beep(count=1)
		
	# Begin moving to the second destination
	working=True
	while working:

		# Read most recent frame
		image = wvs.read()
	
		# Get the arrow mask which will later be used to filter out the arrow from the shapes segmentation	
		arrow_mask = seg.get_arrow_negative(image, black_ref, brown_mask)

		# Extract just the arrow on its own.
		arrow = seg.extract_arrow(arrow_mask)
		arrow_image = arrow[0]						# Extract the arrow binary image
		arrow_head_coordinates = arrow[1]			# Extract the arrow head coordinates
		arrow_tail_coordinates = arrow[2]			# Extract the arrow tail coordinates

		#  Command robot
		angle = control.compute_rotation(image_equalized, arrow_head_coordinates, arrow_tail_coordinates, match_destination)
		distance = control.compute_displacement(arrow_head_coordinates, match_destination)

		# If the robot angle is offset more than the threshold, rotate the robot
		if abs(angle) > angle_threshold:
			robot.steer_in_place(angle)

		# If the robot is still too far away from the destination, then move the robot forward
		if distance > distance_threshold:
			robot.move_forward(30)
	
		# If we are close enough to the robot, stop moving and prepare to go to the new destination (The matching shape)
		if distance <= distance_threshold: 
			working = False
			
			# Beep twice to indicate that we have successfully dropped off a piece
			robot.beep(count=2)

# Finally, we need to go home and then beep twice. The home shape matches the very last element of the shapes data structure
match_destination = [shapes_x_coordinates[shape_match_map[-1], shapes_y_coordinates[shapes_match_map[-1]]]]

# Begin moving to the home destination
working = True
while working:

	# Read most recent frame
	image = wvs.read()
	
	# Get the arrow mask which will later be used to filter out the arrow from the shapes segmentation	
	arrow_mask = seg.get_arrow_negative(image, black_ref, brown_mask)

	# Extract just the arrow on its own.
	arrow = seg.extract_arrow(arrow_mask)
	arrow_image = arrow[0]						# Extract the arrow binary image
	arrow_head_coordinates = arrow[1]			# Extract the arrow head coordinates
	arrow_tail_coordinates = arrow[2]			# Extract the arrow tail coordinates

	#  Command robot
	angle = control.compute_rotation(image_equalized, arrow_head_coordinates, arrow_tail_coordinates, match_destination)
	distance = control.compute_displacement(arrow_head_coordinates, match_destination)

	# If the robot angle is offset more than the threshold, rotate the robot
	if abs(angle) > angle_threshold:
		robot.steer_in_place(angle)

	# If the robot is still too far away from the destination, then move the robot forward
	if distance > distance_threshold:
		robot.move_forward(30)
	
	# If we are close enough to the robot, stop moving and prepare to go to the new destination (The matching shape)
	if distance <= distance_threshold: 
		working = False


# Beep three times to indicate that we have successfully navigated the course
robot.beep(count=3)	
