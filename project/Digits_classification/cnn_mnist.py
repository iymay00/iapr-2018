# -*- coding: utf-8 -*-


import numpy as np 
import matplotlib.pyplot as plt 
# %matplotlib inline
import torch
import torch.nn as nn
from torchvision import datasets, transforms
import pickle
from model_cnn import *
        
def train(model, train_dataset, test_dataset, batch_size, num_epochs):
    '''Train the model'''
    # Data Loader: load data using batches of batch_size samples. In train
    # shuffle imput on each epoch
    train_loader = torch.utils.data.DataLoader(dataset=train_dataset, 
                                               batch_size=batch_size, 
                                               shuffle=True)

    test_loader = torch.utils.data.DataLoader(dataset=test_dataset, 
                                              batch_size=batch_size, 
                                              shuffle=False)


    # Loss and Optimizer
    criterion = nn.CrossEntropyLoss()  # probabilities
    optimizer = torch.optim.Adam(model.parameters())  #fancy SGD

    # Move to GPU
    if torch.cuda.is_available():
        criterion = criterion.cuda()
        model = model.cuda()


    # Train the Model
    model.train() # train mode (doesn't affect in this case)
    acc_train = [] 
    acc_test = []
    
    # Iterate trough epochs
    for epoch in range(num_epochs):
        nb_correct_train = 0
        # Iterate through minibatches
        for i, (inputs, labels) in enumerate(train_loader):  
            print(inputs.shape)
            # Move to GPU
            if torch.cuda.is_available():
                inputs = inputs.cuda()
                labels = labels.cuda()
            
            #Forward pass
            optimizer.zero_grad()  # zero the gradient buffer
            scores = model(inputs) # obtain output
            loss = criterion(scores, labels) #compute loss

            #Backward pass
            loss.backward() #compute gradients of backward pass
            optimizer.step() #sgd step


            #Scores
            pred = scores.data.max(1)[1] # prediction is class with maximum probability
            nb_correct = (pred==labels.data).sum() #number of correctly classified labels
            nb_correct_train += nb_correct


        # Test the Model
        model.eval() # eval mode
        nb_correct_test = 0
        for i, (inputs, labels) in enumerate(test_loader):  
            
            # Move to GPU
            if torch.cuda.is_available():
                inputs = inputs.cuda()
                labels = labels.cuda()

            #Obtain prediction and score
            scores = model(inputs) # output
            pred = scores.data.max(1)[1] # # prediction is class with maximum probability
            nb_correct_test += (pred==labels.data).sum()

        print('  >> Epoch [%d/%d]: Train accuracy: %.4f, Test accuracy %.4f' %(epoch+1, num_epochs, 
                                                                                       100*nb_correct_train/len(train_dataset),
                                                                                       100*nb_correct_test/len(test_dataset)))

        acc_train.append(100*nb_correct_train/len(train_dataset))
        acc_test.append(100*nb_correct_test/len(test_dataset))
    return acc_train, acc_test

    

# Parameters 
input_size = 784
num_hidden = 200
output_size = 10
num_epochs = 150
batch_size = 200
learning_rate = 0.001

# Transform with random flip (p=0.3), rotation between 0 and 90, translation in x and y between 0-15%
# and random rescale between 0.5 (half size) and 1 (don't do anything)
transf = transforms.Compose([
     transforms.RandomVerticalFlip(p=0.35),
     transforms.RandomAffine(90, translate=(0.15,0.15), scale=(0.54,1)),
     transforms.ToTensor(),
])

# MNIST Dataset: train and test applying the transforms
train_dataset = datasets.MNIST(root='../data', 
                            train=True, 
                            transform=transf,  
                            download=True)

test_dataset = datasets.MNIST(root='../data', 
                           train=False, 
                           transform=transf)



# Create the model
model = CNN_simple(num_hidden)
# Train
acc_train, acc_test = train(model, train_dataset, test_dataset, batch_size, num_epochs)

# Save results
with open('cnn_simple.pkl', wb) as f:
    pickle.dump({'acc_train':acc_train, 'acc_test':acc_test},f )
torch.save(model.cpu(),'model_cnn_simple.pt')
torch.save(model.cpu().state_dict(),'model_cnn_simple_params.pt')
