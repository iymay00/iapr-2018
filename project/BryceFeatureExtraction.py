#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import os
import skimage.io
import matplotlib.pyplot as plt

######### Freeman Code ###########
#############################################
# Define a function to determine the freeman code for each image in a class 
#############################################
def freeman_code(images):
    
    # The freeman codes for the set of images
    freeman_codes = []
    
    for im in images:        
        
        # The code for the current image
        current_code = []
        
        # Find the contour for the current image
        contour = detect_contour(im)
        
        # Walk along the contour and determine the freeman code.
        for i in range(0, contour.shape[0]):
            
            # Compute the angle between the two consecutive pixel coordinates.
            current_pixel = contour[i]
            if i == contour.shape[0]-1:
                final_pixel = contour[0]
            
            else:
                final_pixel = contour[i+1]
                
            # Build the freeman code 
            if np.array_equal(final_pixel, np.add(current_pixel, [1,0])): 
                current_code.append(0)
                    
            elif np.array_equal(final_pixel, np.add(current_pixel, [1,1])):
                current_code.append(1)

            elif np.array_equal(final_pixel, np.add(current_pixel, [0,1])):
                current_code.append(2)                
                
            elif np.array_equal(final_pixel, np.add(current_pixel, [-1,1])):
                current_code.append(3)            
            
            elif np.array_equal(final_pixel, np.add(current_pixel, [-1,0])):
                current_code.append(4)
                
            elif np.array_equal(final_pixel, np.add(current_pixel, [-1,-1])):
                current_code.append(5)

            elif np.array_equal(final_pixel, np.add(current_pixel, [0,-1])):
                current_code.append(6)
                
            elif np.array_equal(final_pixel, np.add(current_pixel, [1,-1])):
                current_code.append(7)
 
        # Store the completed code for the current image.
        freeman_codes.append(current_code)
    
    # Return the list of freeman codes
    return freeman_codes

#############################################
# Define a function to perform the Fisher-Wagner algorithm 
# on two different freemen codes
#############################################
def fisher_wagner(image_freeman, reference_freeman):
    
    # Create the table that we will use to perform the fisher-wagner algorithm.
    fw_table = np.zeros((len(image_freeman), len(reference_freeman)))
    
    # First determine the distance of any first string to an empty second string
    for i in range(0, len(image_freeman)):
        fw_table[i,0] = i 
        
    for i in range(0, len(reference_freeman)):
        fw_table[0,i] = i 
    
    # Fill the rest of the table
    for j in range(1, len(reference_freeman)):
        
        for i in range(1, len(image_freeman)):
            
            if image_freeman[i] == reference_freeman[j]:
                
                # The codes match at this index, so no operation required
                fw_table[i][j] = fw_table[i-1][j-1]
                
            else:
                # The distance cost due to an deletion
                deletion = fw_table[i-1, j] + 1
            
                # The distance cost due to an insertion
                insertion = fw_table[i, j-1] + 1  

                # The distance cost due to an substitution
                substitution = fw_table[i-1, j-1] + 1 
            
                # The current position of the table becomes the minimum of these three possible operations
                fw_table[i][j] = np.amin([deletion, insertion, substitution])
            
    # The edition distance is the bottom-right value of this table
    return fw_table[-1][-1]

    
#############################################
# Define a function to extract minimum edition distance 
# for each number with respect to a given image 
#############################################
def edition_distances(class_freeman_codes, reference_freeman):
    
    # The distances for each image.
    edition_distances = np.empty((len(class_freeman_codes),1))
    
    for i in range(0, len(class_freeman_codes)):
        
        # Determine the freemand code for the current image
        current_freeman = class_freeman_codes[i]
        
        # Compute the edition distance between image's freemand code and the reference freemand code via fisher-wagner.
        edition_distance = fisher_wagner(current_freeman, reference_freeman)
        
        # Update the vector of edition distances.
        edition_distances[i] = edition_distance
    
    return edition_distances


######### Fourier Descriptors ####
from skimage.measure import find_contours
from skimage.filters import threshold_mean
from skimage.filters import median
from skimage.morphology import binary_dilation
from skimage.morphology import binary_erosion


def detect_contour(image):
    
    # Median filter the  images to filter out noise and smoothen the image.
    image = median(image)
    
    # Find a threshold for the zero and one images
    thresh = threshold_mean(image)
    
    # Create binary images for each picture
    binary_image = image > thresh
     
    # Perform binary closing on the image, which is a dilation followed by an erosion operation
    binary_image = binary_dilation(binary_image)
    binary_image = binary_erosion(binary_image)
    # binary_image = binary_closing(binary_image)
    
    # Find the contours of the images. Contours are represented by an array of coordinates (x, y)
    contour = find_contours(binary_image, 0)[0]
    
    return contour

#############################################
# Define a function to extract the fourier descriptions for a list of images. 
#############################################

def extract_fourier_descriptors(images, names, descriptors_to_extract, plot_contours):
    
    # Count the number of images to process
    num = images.shape[0]
    
    # Data structures to hold the contours of the numbers and the extracted descriptors.
    contours = []
    fourier_descriptors = np.empty( (num, 2), dtype = complex)

    # Compute the fourier transform of each image
    for i in range(0, num): 
        
        # Find the contour of the images
        contour_temp = detect_contour(images[i])
    
        # Create a n x 2 array of the contour coordinates for each image.
        contours.append(contour_temp)
    
        # Convert each coordinate into a single complex number instead of tuples. 
        # Perform a DFT on this array of coordinates represented as a list of complex numbers. 
        dft = np.fft.fft(contour_temp[:,0] + 1j*contour_temp[:,1])
    
        # Extract the 2nd & 3rd fourier descriptors as the features of interest. 
        # The descriptors are a series of complex numbers compted by taking the DFT of the original contours.
        fourier_descriptors[i] = [dft[descriptors_to_extract[0]], dft[descriptors_to_extract[1]]]
   
    if plot_contours:
        # Plot the contours on top of the original images
        fig, axes = plt.subplots(1, num, figsize=(16, 4))
        plt.suptitle('Contour Detection', fontsize=30)
        for ax, im, cont, nm in zip(axes, images, contours, names):
            ax.plot(cont[:,1], cont[:,0], linewidth=3)
            ax.imshow(im, cmap=plt.cm.gray, alpha=1, interpolation='bilinear')
            ax.axis('off')
            ax.set_title(nm)
        plt.show()    
    
    # Return the extracted fourier descriptors.
    return fourier_descriptors # array

######### Distance Map ###########
from scipy.ndimage import distance_transform_cdt

distance_transform_cdt(ref_img)

def compute_dist_map(ref_img, contour): # reference image, contour coordinates
    # given contour (image?)
    # for each point in image compute dist
    # how far out to go?
    img_x = len(ref_img[0])
    img_y = len(ref_img[1])
    
    dist = np.ones(img_x, img_y)*2**62
ss    for k in range(len(contour)):
        dist[contour[k]] = 0
        
    for i in range(img_x):
        for j in range(img_y):
            if dist(i,j) == 0: contour_found = True
            if contour_found:
                down  = dist[i][j-1]   + 3
                right = dist[i-1][j]   + 3
                diag  = dist[i-1][j-1] + 4
                dist[i][j] = min(down, right, diag, dist[i][j])
    
    for i in range(img_x-1,-1,-1):
        for j in range(img_y-1,-1,-1):
            if dist(i,j) == 0: contour_found = True
            if contour_found:
                up   = dist[i][j+1]   + 3
                left = dist[i+1][j]   + 3
                diag = dist[i+1][j+1] + 4
                dist[i][j] = min(up, left, diag, dist[i][j])
            
