#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import os
import matplotlib.pyplot as plt
import math
import segmentation_functions as seg


def compute_rotation(robot_head, robot_tail, destination_point):

	destination_x = destination_point[0]
	destination_y = destination_point[1]
	
	# Compute angle of the destination point with respect to the tail.
	destination_angle = seg.get_arrow_angle(image, destination_point, robot_tail)
	
	# Compute angle of the robot head with respect to the tail.
	robot_angle = seg.get_arrow_angle(image, robot_head, robot_tail)
	
	rotation = destination_angle - robot_angle
	
	
	return rotation
	
	
	
	
def compute_displacement(robot_head, destination_point):

	displacement = np.linalg.norm(robot_head) - np.linalg.norm(robot_head)
	
	
	return displacement
		
		



	
	
	
	

