
import numpy as np 
import matplotlib.pyplot as plt 
# %matplotlib inline
import torch
from skimage.transform import resize

def obtain_prediction(model, digits, plot = False):
    
    out = []

    for input_ in digits:
        # If shape is empty return -1
        if input_.sum()<50:
            out.append(-1)
        else:
            # Approximate bounding box of input
            # Nonzero positions
            nzero= input_.nonzero()
           
            # Range where we have the number +- 15  of safety
            var_range = int((np.max(nzero) - np.min(nzero) + input_.shape[0]/4)/2)
            
            # Approximate center bounding box
            center = (int((nzero[0][0]+nzero[0][-1])/2), 
                   int((nzero[1][0]+nzero[1][-1])/2))
  

            # Crop input
            input_ = input_[max(0,center[0]-var_range):center[0]+var_range,
                            max(0,center[1]-var_range):center[1]+var_range]

            # Resize to 28x28                        
            input_ = resize(input_, (28,28))
            # Convert to tensor and add dummy dimensions
            input_ = torch.Tensor(input_).view(1,1,28,28)
            
            
            # Obtain output
            scores = model(input_)
            out.append(scores.data.max(1)[1].contiguous().numpy()[0]) # prediction is class with maximum probability
            plot=False
            if plot:
                plt.imshow(input_.view(28,28))
                plt.title('Predicted: {}'.format(out[-1]))
                plt.show()
                plt.pause(2)
                
    return out
    
