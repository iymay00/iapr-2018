#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import os
import skimage.io
import matplotlib.pyplot as plt
import segmentation_functions as seg
import math
import matching_functions as m

# Set some macros to control plotting
plot_raw = False
plot_normalize = False
plot_shapes_mask = False
plot_digits = False
plot_shapes = False
plot_arrow = False
plot_matches = True


image_sel = 25

# Directory of the images relative to the current directory
data_base_path = os.path.join(os.pardir, 'data')
data_folder = 'Parcours_updated'
data_path = os.path.join(data_base_path, data_folder)

#print "Loading the images..."

# Load images. Make sure they are in the LAB color space
number_of_images = 32
im_names = [str(i) for i in range(1,number_of_images + 1,1)]

filenames = [os.path.join(data_path, name) + '.jpg' for name in im_names]
ic = skimage.io.imread_collection(filenames)
images = skimage.io.concatenate_images(ic)

black_ref = skimage.io.imread(os.path.join(data_path,'black_ref')+'.jpg')

# Select an image to send through the pipeline
image = images[image_sel]

# Plot images, select whether to plot
if plot_raw:
	fig, ax = plt.subplots(1, 1, figsize=(6, 6))
	ax.imshow(image)
	ax.axis('off')

# Normalize the intensities of the images.
#print "Normalizing the image intensity..."
image_equalized = seg.intensity_normalization(image)
if plot_normalize:
	fig, ax = plt.subplots(1, 1, figsize=(6, 6))
	ax.imshow(image_equalized)
	ax.axis('off')

brown_mask = seg.remove_brown_background(image_equalized)

# Get the arrow mask which will later be used to filter out the arrow from the shapes segmentation	
arrow_negative = seg.get_arrow_negative(image, black_ref, brown_mask)

# Segment the image shapes (excluding the black arrow)
#print "Performing segmentation..."
shapes_mask = seg.segment_image(image_equalized, arrow_negative, brown_mask)
if plot_shapes_mask:
	fig, axes = plt.subplots(1, 2, figsize=(12, 12))
	for ax, im in zip(axes.ravel(), [images[image_sel], shapes_mask]):
		ax.imshow(im, cmap='gray')
		ax.axis('off')
     
#print "Extracting shapes..."
shapes_extraction = seg.extract_shapes(image_equalized, shapes_mask)
shapes = shapes_extraction[0]
if plot_shapes:
	for c in range(0, len(shapes)):
		fig, ax = plt.subplots(1, 1, figsize=(6, 6))
		ax.imshow(shapes[c], cmap='gray')
		ax.axis('off')
		ax.set_title("A Crop of Dimensions: " + str(len(shapes[c][:,1])) + " x " + str(len(shapes[c][1,:])))


#print "Extracting digits..."
digits_extraction = seg.extract_numbers(image, shapes_extraction, shapes_mask)
digits = digits_extraction[0]
if plot_digits:
	for c in range(0, len(digits)):
		fig, ax = plt.subplots(1, 1, figsize=(6, 6))
		ax.imshow(digits[c], cmap='gray')
		ax.axis('off')
		ax.set_title("A Crop of Dimensions: " + str(len(digits[c][:,1])) + " x " + str(len(digits[c][1,:])))
				  				
# Extract just the arrow on its own.
arrow = seg.extract_arrow(arrow_negative)
arrow_image = arrow[0]						# Extract the arrow binary image
arrow_head_coordinates = arrow[1]			# Extract the arrow head coordinates
arrow_tail_coordinates = arrow[2]			# Extract the arrow tail coordinates
	
# Get the arrow angle
arrow_angle = seg.get_arrow_angle(image, arrow_head_coordinates, arrow_tail_coordinates)

if plot_arrow:

	fig, ax = plt.subplots(1, 2, figsize=(6, 6))
	ax[0].imshow(arrow_image, cmap='gray')
	ax[0].axis('off')
	ax[0].set_title("Robot Arrow angle:" + str(arrow_angle) + " degrees. Head: " + str(arrow_head_coordinates) + " Tail: " + str(arrow_tail_coordinates))
	ax[1].imshow(image)
	ax[1].axis('off')


# SHAPE MATCHING
match_list = m.match_shapes(shapes)

if plot_matches:
	for c in range(0, len(shapes)):
		fig, ax = plt.subplots(1, 2, figsize=(6, 6))
		ax[0].imshow(shapes[c], cmap='gray')
		ax[0].axis('off')
		ax[0].set_title("Image " + str(c))
		ax[1].imshow(shapes[match_list[c]], cmap='gray')
		ax[1].axis('off')
		ax[1].set_title("Image Matched to Image " + str(c))
		
	
	
# Show all plots
plt.show()
