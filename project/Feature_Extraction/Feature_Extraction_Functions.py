#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

######################################
######### Freeman Code ###############
######################################

# compute freeman code
def freeman_code(images):
    
    # The freeman codes for the set of images
    freeman_codes = []
    
    for im in images:        
        
        # The code for the current image
        current_code = []
        
        # Find the contour for the current image
        contour = detect_contour(im)
        
        # Walk along the contour and determine the freeman code.
        for i in range(contour.shape[0]):
            
            # Compute the angle between the two consecutive pixel coordinates.
            current_pixel = contour[i]
            if i == contour.shape[0]-1:
                final_pixel = contour[0]
            
            else:
                final_pixel = contour[i+1]
                
            # Build the freeman code 
            if np.array_equal(final_pixel, np.add(current_pixel, [1,0])): 
                current_code.append(0)
                    
            elif np.array_equal(final_pixel, np.add(current_pixel, [1,1])):
                current_code.append(1)

            elif np.array_equal(final_pixel, np.add(current_pixel, [0,1])):
                current_code.append(2)                
                
            elif np.array_equal(final_pixel, np.add(current_pixel, [-1,1])):
                current_code.append(3)            
            
            elif np.array_equal(final_pixel, np.add(current_pixel, [-1,0])):
                current_code.append(4)
                
            elif np.array_equal(final_pixel, np.add(current_pixel, [-1,-1])):
                current_code.append(5)

            elif np.array_equal(final_pixel, np.add(current_pixel, [0,-1])):
                current_code.append(6)
                
            elif np.array_equal(final_pixel, np.add(current_pixel, [1,-1])):
                current_code.append(7)
 
        # Store the completed code for the current image.
        freeman_codes.append(current_code)
    
    # Return the list of freeman codes
    return freeman_codes

# Define a function to perform the Fisher-Wagner algorithm 
# on two different freemen codes
def fisher_wagner(image_freeman, reference_freeman):
    
    # Create the table that we will use to perform the fisher-wagner algorithm.
    fw_table = np.zeros((len(image_freeman), len(reference_freeman)))
    
    # First determine the distance of any first string to an empty second string
    for i in range(0, len(image_freeman)):
        fw_table[i,0] = i 
        
    for i in range(0, len(reference_freeman)):
        fw_table[0,i] = i 
    
    # Fill the rest of the table
    for j in range(1, len(reference_freeman)):
        
        for i in range(1, len(image_freeman)):
            
            if image_freeman[i] == reference_freeman[j]:
                
                # The codes match at this index, so no operation required
                fw_table[i][j] = fw_table[i-1][j-1]
                
            else:
                # The distance cost due to an deletion
                deletion = fw_table[i-1, j] + 1
            
                # The distance cost due to an insertion
                insertion = fw_table[i, j-1] + 1  

                # The distance cost due to an substitution
                substitution = fw_table[i-1, j-1] + 1 
            
                # The current position of the table becomes the minimum of these three possible operations
                fw_table[i][j] = np.amin([deletion, insertion, substitution])
            
    # The edition distance is the bottom-right value of this table
    return fw_table[-1][-1]

    
# Define a function to extract minimum edition distance 
# for each number with respect to a given image 
def edition_distances(reference_freeman, class_freeman_codes):
    
    # The distances for each image.
    edition_distances = np.empty((len(class_freeman_codes),1))
    
    for i in range(len(class_freeman_codes)):
        
        oriented = False
        fc_length = len(class_freeman_codes[i])
        step_size = 10
        dist_traversed = 0
        best_score = 2**62
        while(~oriented):
            # Rotate freeman code
            
            
            # Determine the freemand code for the current image
            current_freeman = class_freeman_codes[i]
        
            # Compute the edition distance between image's freemand code and the reference freemand code via fisher-wagner.
            edition_distance = fisher_wagner(current_freeman, reference_freeman)
            
            # update best orientation
            if edition_distance < best_score:
                best_score=edition_distance
            
            current_freeman = current_freeman[step_size:] + current_freeman[:step_size]
            dist_traversed += step_size
            
            if dist_traversed >= fc_length:
                break
        # Update the vector of edition distances.
        edition_distances[i] = best_score
    
    return edition_distances

######################################
######### Fourier Descriptors ########
######################################
from skimage.measure import find_contours
from skimage.filters import threshold_mean
from skimage.filters import median
from skimage.morphology import binary_dilation
from skimage.morphology import binary_erosion
#from skimage.morphology import binary_closing


def detect_contour(image):
    
    # Median filter the  images to filter out noise and smoothen the image.
    image = median(image)
    
    # Find a threshold for the zero and one images
    thresh = threshold_mean(image)
    
    # Create binary images for each picture
    binary_image = image > thresh
     
    # Perform binary closing on the image, which is a dilation followed by an erosion operation
    binary_image = binary_dilation(binary_image)
    binary_image = binary_erosion(binary_image)
    # binary_image = binary_closing(binary_image)
    
    # Find the contours of the images. Contours are represented by an array of coordinates (x, y)
    contour = find_contours(binary_image, 0)[0]
    
    return contour

# Extract the fourier descriptions for a list of images. 
def extract_fourier_descriptors(images, descriptors_to_extract, plot_contours=False):
    
    # Count the number of images to process
    num = len(images)
    
    # make sure we don't take more descriptors than are in the image
    for i in range(num): 
        contour_temp = detect_contour(images[i])
        if len( np.fft.fft(contour_temp[:,0]) ) < len(descriptors_to_extract):
            descriptors_to_extract = range(len( np.fft.fft(contour_temp[:,0]) ))

    
    # Data structures to hold the contours of the numbers and the extracted descriptors.
    contours = []
    fourier_descriptors = np.ones( (num, len(descriptors_to_extract)), dtype = complex)
    fourier_descriptors_out = np.ones( (num, len(descriptors_to_extract)-1), dtype = complex)
    # Compute the fourier transform of each image
    for i in range(num): 
        
        # Find the contour of the images
        contour_temp = detect_contour(images[i])
    
        # Create a n x 2 array of the contour coordinates for each image.
        contours.append(contour_temp)
    
        # Convert each coordinate into a single complex number instead of tuples. 
        # Perform a DFT on this array of coordinates represented as a list of complex numbers. 
        dft = np.fft.fft(contour_temp[:,0] + 1j*contour_temp[:,1])
        fourier_descriptors[i] = dft[descriptors_to_extract]

        # scale the fourier descriptors to make results scale invariant
        fourier_descriptors[i] = fourier_descriptors[i]/max(abs(fourier_descriptors[i]))
        fourier_descriptors[i] /= fourier_descriptors[i][0]
        #fourier_descriptors[i] = np.linalg.norm(fourier_descriptors[i])]
        
        fourier_descriptors_out[i] = fourier_descriptors[i][1:]
        
    # Return the extracted fourier descriptors.
    return fourier_descriptors_out # array



from skimage import transform as tf
def shape_similarity_tf(shape1, shape2):
    size1 = np.sum(shape1)
    size2 = np.sum(shape2)
    if size1 >= size2: 
        # scale img2 to be same size as img1
        tform = tf.SimilarityTransform(scale=size1/size2)

        tform.estimate(shape1, shape2)
        img_scaled = tform(shape2)

        # crop img2
        img_cropped = img_scaled[shape2.shape[1], shape2.shape[0]]
        
        return img_cropped

    elif size1 <= size2: 
        # scale img1 to be same size as img2
        tform = tf.SimilarityTransform(scale=size2/size1)

        tform.estimate(shape2, shape1)
        img_scaled = tform(shape1)
	
        # crop img2
        img_cropped = img_scaled[shape1.shape[1], shape1.shape[0]]
        
        return img_cropped
    
def get_numbered_shape_index(shapes, digits):

    unnumbered_threshold = 50
    if (len(shapes) != len(digits)):
        return "error: size mismatch"
    sums = []
    
    for i in range(len(digits)):
        sums.append(np.sum(digits[i]))
#		if sums[i] < 50
#			indices
    indices = np.nonzero(sums)
    return indices[0]

def get_unnumbered_shape_index(shapes, digits):
    if (len(shapes) != len(digits)):
        return "error: size mismatch"
    nsums = []
    for i in range(len(digits)):
        if np.sum(digits[i]): nsums.append(0)
        else: nsums.append(1)
    indices = np.nonzero(nsums)
    return indices[0]
            
def morph_skel_dist(morph1, morph2):
    pass

from skimage.measure import perimeter
def calculate_compactness(images):
    images = np.asarray(images)

    if len(images.shape) == 2:
        return np.sum(images)/perimeter(images)**2
    
    if len(images.shape) == 3:
        compactness = []
        for i in range(images.shape[0]):
            compactness.append(np.sum(images[i])/perimeter(images[i])**2)
        return compactness


# Hough transform
from skimage.transform import hough_line
def hough_transform(images):
    images = np.asarray(images)
    
    if len(images.shape) == 2:
        
        out, angles, d = hough_line(images)
        
        return [out, angles, d]
    
    if len(images.shape) == 3:
        h_transforms = []
        for i in range(images.shape[0]):
            
            out, angles, d = hough_line(images[i])
            
            h_transforms.append([out,angles,d])
        
        h_transforms = np.asarray(h_transforms)
            
        return h_transforms



def classify_images(segments, digits):

    # import images and labels
    #segments = shapes # contains segmented binary images and labels

    ######### Standardize the images #################
    # get area of each segmented shape
    sums = []
    for i in range(len(segments)):
        sums.append(np.sum(segments[i]))

    ##############################
    # GET NUMBERED SHAPES ########
    ##############################

    # Separate all shapes that have numbers from those that don't
    #numbered_indices = get_numbered_shape_index(segments, digits)
    numbered_indices = (digits != -1).nonzero()[0]
    unnumbered_indices = (digits == -1).nonzero()[0]
    numbered_segs = []
    for i in range(len(numbered_indices)):
        numbered_segs.append( segments[numbered_indices[i]] )

    #unnumbered_indices = get_unnumbered_shape_index(segments, digits)
    unnumbered_segs = []
    for i in range(len(unnumbered_indices)):
        unnumbered_segs.append( segments[unnumbered_indices[i]] )
    print(len(unnumbered_segs))
    ###### Identify Home #####
    # create reference circle
    circle_img_size = segments[0].shape
    circle_img = np.zeros(circle_img_size)
    np.sqrt(np.mean(sums)/np.pi)
    circle_radius = np.sqrt(np.mean(sums)/np.pi)
    circle_center = circle_img_size[0]//2
    for i in range(circle_img_size[0]):
        for j in range(circle_img_size[1]):
            if (i-circle_center)**2 + (j-circle_center)**2 <= circle_radius**2 :
                circle_img[i,j] = 1

    numbered_segs.append( circle_img )
    print(numbered_indices)
    numbered_indices = np.append(numbered_indices, len(digits))
    print(numbered_indices)
    circle_label = 500

    # compare circle image to unnamed objects to find home
    circle_score = 200**2
    for i in range(len(unnumbered_segs)):
        score = np.sum( (unnumbered_segs[i]-circle_img)**2 )
        if score < circle_score:
            circle_score = score
            home_index_unnumbered = i
        
 
    ##################################################
    ########### Calculate Feature Values #############
    ##################################################

    descriptors = range(1,100)

    print("calculating features")

    # calculate freeman code
    #numbered_freeman = feat.freeman_code(numbered_segs)
    #unnumbered_freeman = feat.freeman_code(unnumbered_segs) 

    # extract fourier descriptors
    numbered_f_descriptors = extract_fourier_descriptors(numbered_segs, descriptors) 
    numbered_f_descriptors = np.abs(numbered_f_descriptors)

    unnumbered_f_descriptors = extract_fourier_descriptors(unnumbered_segs, descriptors) 
    unnumbered_f_descriptors = np.abs(unnumbered_f_descriptors)

    # compactness is area/perimeter**2
    # compactness values are b/w 0.04 & 0.07
    numbered_compactness   = calculate_compactness(numbered_segs)
    unnumbered_compactness = calculate_compactness(unnumbered_segs)
       
    ################################# Combination ################################
    # Compute distances
    distance_fd=[]
    distance_com=[]
    for fd_num, com_num in zip(numbered_f_descriptors, numbered_compactness):
        distance_fd_un = []
        distance_com_un = []
    
        for fd_unnum, com_unnum in zip(unnumbered_f_descriptors,unnumbered_compactness):
            distance_fd_un.append( np.sum(  (fd_num - fd_unnum)**2) )
            distance_com_un.append(np.sqrt( (com_num - com_unnum)**2) )
        
        distance_fd.append(distance_fd_un)
        distance_com.append(distance_com_un)

    # Convert to array
    distance_com = np.array(distance_com)
    distance_fd  = np.array(distance_fd)

    # min-max normalization of distances
    distance_com = (distance_com - np.min(distance_com)) / (np.max(distance_com) - np.min(distance_com))
    distance_fd  = (distance_fd  - np.min(distance_fd))  / (np.max(distance_fd)  - np.min(distance_fd))


    # Indices of unnumbered that minimize distance to numbered
    percent_fd = 0.5
    percent_com = 1-percent_fd

    idx = np.argmin(percent_fd*distance_fd + percent_com*distance_com, axis = 1)
    
    output = np.empty(len(digits)+1, dtype='int')
    
    for i in range(len(numbered_segs)):
        output[numbered_indices[i]] = unnumbered_indices[idx[i]]
        output[unnumbered_indices[idx[i]]] = numbered_indices[i]
        #output.append([numbered_indices[i], unnumbered_indices[idx[i]]])

    #output = np.array(output)
    return output

