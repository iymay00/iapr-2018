#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
os.chdir('/Users/bryceburgess/Documents/EPFLimageAnalysis/iapr-2018/labs/')
# %%
######## EXTRACT DATA #######################

import tarfile
import os

data_base_path = os.path.join(os.pardir, 'data')
data_folder = 'lab-03-data'
tar_path = os.path.join(data_base_path, data_folder + '.tar.gz')
with tarfile.open(tar_path, mode='r:gz') as tar:
    tar.extractall(path=data_base_path)

# %%
######### 1 LOAD DATA ########################
import scipy.io
import numpy as np
import matplotlib.pyplot as plt
import pylab as pl


data_part1_path = os.path.join(data_base_path, data_folder, 'part1', 'classification.mat')
matfile = scipy.io.loadmat(data_part1_path)
a = matfile['a']
b = matfile['b']
c = matfile['c']

print(a.shape, b.shape, c.shape, )

plt.scatter(a[:,0], a[:,1], c='r', marker='^')
plt.scatter(b[:,0], b[:,1], c='b', marker='o')
plt.scatter(c[:,0], c[:,1], c='g', marker='s')
plt.show()

#%%
################ Gaussian Regions ##################


x_max = int( np.ceil( max( max(a[:,0]), max(b[:,0]), max(c[:,0]))  ) )
x_min = int( np.floor( min( min(a[:,0]), min(b[:,0]), min(c[:,0])) ) )
y_max = int( np.ceil( max( max(a[:,1]), max(b[:,1]), max(c[:,1]))  ) )
y_min = int( np.floor( min( min(a[:,1]), min(b[:,1]), min(c[:,1])) ) )

def gaussian(mean, std, x):
    return 1/(std * np.sqrt(2*np.pi)) * np.exp(-0.5*( (mean-x)/std) **2) 

a_mean = np.mean(a, axis = 0)
a_std = np.std(a, axis = 0)
np.sqrt( np.cov(a[:,0], a[:,1]) )

b_mean = np.mean(b, axis = 0)
b_std = np.std(b, axis = 0)
np.sqrt( np.cov(b[:,0], b[:,1]) )

c_mean = np.mean(c, axis = 0)
c_std = np.std(c, axis = 0)
np.sqrt( np.cov(c[:,0], c[:,1]) )

separation_pts_x = []
separation_pts_y = []

thresh = 0.0005
resolution = 0.1
for x in pl.frange(x_min, x_max, resolution):
    for y in pl.frange(y_min, y_max, resolution):
        a_prob = gaussian(a_mean[0], a_std[0], x) * gaussian(a_mean[1], a_std[1], y)
        b_prob = gaussian(b_mean[0], b_std[0], x) * gaussian(b_mean[1], b_std[1], y)
        c_prob = gaussian(c_mean[0], c_std[0], x) * gaussian(c_mean[1], c_std[1], y)
    
        if (a_prob <= b_prob and a_prob <= c_prob):# something wrong here
            if abs(b_prob-c_prob) < thresh:
                separation_pts_x.append(x)
                separation_pts_y.append(y)
        
        if (c_prob <= a_prob and c_prob <= b_prob):
            if ((abs(a_prob-b_prob) < thresh) >= c_prob):
                separation_pts_x.append(x)
                separation_pts_y.append(y) 
                
        if (b_prob <= a_prob and b_prob <= c_prob):
            if ((abs(a_prob-c_prob) < thresh) >= b_prob):
                separation_pts_x.append(x)
                separation_pts_y.append(y)
                
plt.scatter(a[:,0], a[:,1], c="r", marker="^")
plt.scatter(b[:,0], b[:,1], c="b", marker="o")
plt.scatter(c[:,0], c[:,1], c="g", marker="s")
plt.scatter(separation_pts_x, separation_pts_y, c="k", marker=".")

#%%
############ LINEAR AND QUADRATIC MODELS
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as lda

# all three labeled data sets
data_all = np.vstack((a,b,c))
labels_all = np.transpose( tuple([["a"]*len(a) + ["b"]*len(b) + ["c"]*len(c)]) )

linear_model1 = lda()
linear_model1.fit(data_all, labels_all.ravel())
linear_model1.coef_ # what is the order of coefs? why 3 lines?
errors1 = np.count_nonzero(~(linear_model1.predict(data_all) == 
                            np.transpose(labels_all)))

# separate a from b
data_ab = np.vstack((a,b))
labels_ab = np.transpose( tuple([["a"]*len(a) + ["b"]*len(b)]) )

linear_model2 = lda()
linear_model2.fit(data_ab, labels_ab.ravel())
linear_model2.coef_ # what is the order of coeffs? why are there 3?
errors2 = np.count_nonzero(~(linear_model2.predict(data_ab) == 
                            np.transpose(labels_ab)))

# separate a from c
data_ac = np.vstack((a,c))
labels_ac = np.transpose( tuple([["a"]*len(a) + ["c"]*len(c)]) )

linear_model3 = lda()
linear_model3.fit(data_ac, labels_ac.ravel())
linear_model3.coef_ # what is the order of coeffs? why are there 3?
errors3 = np.count_nonzero(~(linear_model3.predict(data_ac) == 
                            np.transpose(labels_ac)))

# separate b from c
data_bc = np.vstack((b,c))
labels_bc = np.transpose( tuple([["b"]*len(b) + ["c"]*len(c)]) )

linear_model4 = lda()
linear_model4.fit(data_bc, labels_bc.ravel())
linear_model4.coef_ # what is the order of coeffs? why are there 3?
errors4 = np.count_nonzero(~(linear_model4.predict(data_bc) == 
                            np.transpose(labels_bc)))

# separate a from b and c
data_a = np.vstack((a,b,c))
labels_a = np.transpose( tuple([["a"]*len(b) + ["xa"]*len(b) + ["xa"]*len(c)]) )

linear_model5 = lda()
linear_model5.fit(data_a, labels_a.ravel())
linear_model5.coef_ # what is the order of coeffs? why are there 3?
errors5 = np.count_nonzero(~(linear_model5.predict(data_a) == 
                            np.transpose(labels_a)))

# separate b from a and c
data_b = np.vstack((a,b,c))
labels_b = np.transpose( tuple([["xb"]*len(b) + ["b"]*len(b) + ["xb"]*len(c)]) )

linear_model6 = lda()
linear_model6.fit(data_b, labels_b.ravel())
linear_model6.coef_ # what is the order of coeffs? why are there 3?
errors6 = np.count_nonzero(~(linear_model6.predict(data_b) == 
                            np.transpose(labels_b)))

# separate c from a and b
data_c = np.vstack((a,b,c))
labels_c = np.transpose( tuple([["xc"]*len(b) + ["xc"]*len(b) + ["c"]*len(c)]) )

linear_model7 = lda()
linear_model7.fit(data_c, labels_c.ravel())
linear_model7.coef_ # what is the order of coeffs? why are there 3?
errors7 = np.count_nonzero(~(linear_model7.predict(data_c) == 
                            np.transpose(labels_c)))


""" #%%
def discriminant(data, x): # data = class data, x = point in feature space
    mean = np.mean(data, axis = 0)
    cov = np.cov(np.transpose(data))
    P = len(data)
    g = []
    for i in range(len(x)):
        g.append( -0.5 * np.dot( np.dot(x,    1/cov), x   ) +
                  -0.5 * np.dot( np.dot(mean, 1/cov), mean) +
                  -0.5 * np.dot( np.dot(mean, 1/cov), x   ) +
                  -0.5 * np.dot( np.dot(x,    1/cov), mean) + np.log(P) )
    return g

def discriminant_classifier(x, data1, data2):
    class1_pts = []
    class2_pts = []
    g1 = discriminant(data1,x)
    g2 = discriminant(data2,x)
    for i in range(len(x)):
        if   g1>g2:
            class2_pts.append( x[i] )
        elif g2<g1:
            class1_pts.append( x[i] )
        elif g1==g2:
            pass
    return [class1_pts, class2_pts]

def discriminant_classifier2(x, data1, data2, data3):
    class1_pts = []
    class2_pts = []
    class3_pts = []

    g1 = discriminant(data1, x)
    g2 = discriminant(data2, x)
    g3 = discriminant(data3, x)

    for i in range(len(x)):
        if min(g1, g2, g3) == g1 :
            class1_pts.append(x[i])
            
        elif min(g1, g2, g3) == g2 :
            class2_pts.append(x[i])
            
        elif min(g1, g2, g3) == g3 :
            class3_pts.append(x[i])
            
    return [[class1_pts], [class2_pts], [class3_pts]]

    
# compare a:b, a:c, b:c, 
resolution = 0.1
for x in pl.frange(x_min, x_max, resolution):
    for y in pl.frange(y_min, y_max, resolution):
        discriminant_classifier2([x,y], a, b, c)

# compare a:bc, b:ac, c:ab
d = np.concatenate((a,b))
e = np.concatenate((a,c))
f = np.concatenate((b,c))
for x in pl.frange(x_min, x_max, resolution):
    for y in pl.frange(y_min, y_max, resolution):
        discriminant_classifier([x,y], a, f)
        discriminant_classifier([x,y], b, e)
        discriminant_classifier([x,y], c, d)
"""

#%%
######### 1.3 Mahalanobis Dist ###########################
# calculate this value for sample points for each class, take the 
from scipy.spatial.distance import mahalanobis as mln

def mahalanobis_dist(x, data): # point(s) to check, single class data
    mean = np.mean(data, axis = 0)
    cov = np.cov(np.transpose(data))
    md = []
    for i in range(len(x)):
        md.append( mln( x[i], mean, cov) ) 
    return md

def mahalanobis_classifier(x, data1, data2, data3):
    class1_pts = []
    class2_pts = []
    class3_pts = []

    for i in range(len(x)):
        md1 = mahalanobis_dist(x[i], data1)
        md2 = mahalanobis_dist(x[i], data2)
        md3 = mahalanobis_dist(x[i], data3) 
    
        if   min(md1, md2, md3) == md1:
            class1_pts.append(x[i])
            
        elif min(md1, md2, md3) == md2:
            class2_pts.append(x[i])
            
        elif min(md1, md2, md3) == md3:
            class3_pts.append(x[i])
                
    return [class1_pts, class2_pts, class3_pts]

n_sample_points = 100
sample_x_resolution = (float(x_max+1)-float(x_min-1))/np.sqrt(n_sample_points)
sample_y_resolution = (float(y_max+1)-float(y_min-1))/np.sqrt(n_sample_points)

sample_region = np.zeros((n_sample_points, 2))
i=0
for j in pl.frange(x_min, x_max, sample_x_resolution):
    for k in pl.frange(y_min, y_max, sample_y_resolution):
        sample_region[i] = (j,k)
#        print i
        i+=1

sample_classified = mahalanobis_classifier(sample_region, a, b, c) 

colors = ("r","b","g")
markers = ("^","o","s")
for i in range(len(sample_classified)):
    for j in range(len(sample_classified[i])):
        plt.scatter(sample_classified[i][j][0], sample_classified[0][i][1], 
                c=colors[i], marker=markers[i])

plt.show()


#%%
###################################################################################
################################# 2 LOAD DATA #####################################
###################################################################################

import gzip

def extract_data(filename, image_shape, image_number):
    with gzip.open(filename) as bytestream:
        bytestream.read(16)
        buf = bytestream.read(np.prod(image_shape) * image_number)
        data = np.frombuffer(buf, dtype=np.uint8).astype(np.float32)
        data = data.reshape(image_number, image_shape[0], image_shape[1])
    return data


def extract_labels(filename, image_number):
    with gzip.open(filename) as bytestream:
        bytestream.read(8)
        buf = bytestream.read(1 * image_number)
        labels = np.frombuffer(buf, dtype=np.uint8).astype(np.int64)
    return labels


#%%
# TRAINING AND TESTING SETS

image_shape = (28, 28)
train_set_size = 60000
test_set_size = 10000

data_part2_folder = os.path.join(data_base_path, data_folder, 'part2')

train_images_path = os.path.join(data_part2_folder, 'train-images-idx3-ubyte.gz')
train_labels_path = os.path.join(data_part2_folder, 'train-labels-idx1-ubyte.gz')
test_images_path = os.path.join(data_part2_folder, 't10k-images-idx3-ubyte.gz')
test_labels_path = os.path.join(data_part2_folder, 't10k-labels-idx1-ubyte.gz')

train_images = extract_data(train_images_path, image_shape, train_set_size)
test_images = extract_data(test_images_path, image_shape, test_set_size)
train_labels = extract_labels(train_labels_path, train_set_size)
test_labels = extract_labels(test_labels_path, test_set_size)


#%%
# VISUALIZE DATA FIRST
prng = np.random.RandomState(seed=123456789)  # seed to always re-draw the same distribution
plt_ind = prng.randint(low=0, high=train_set_size, size=10)

fig, axes = plt.subplots(1, 10, figsize=(12, 3))
for ax, im, lb in zip(axes, train_images[plt_ind], train_labels[plt_ind]):
    ax.imshow(im, cmap='gray')
    ax.axis('off')
    ax.set_title(lb)
    
#%%
#### 2.2 MLP ######################################





