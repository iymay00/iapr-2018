#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import os
import matplotlib.pyplot as plt
import math
import Segmentation.segmentation_functions as seg


def compute_rotation(image, robot_head, robot_tail, destination_point):

	# Compute angle of the destination point with respect to the tail.
	destination_angle = seg.get_arrow_angle(image, destination_point, robot_tail)
	print("Target: " + str(destination_angle))
	
	# Compute angle of the robot head with respect to the tail.
	robot_angle = seg.get_arrow_angle(image, robot_head, robot_tail)
	print("Robot: " + str(robot_angle))

	# Compute the needed rotation as the difference between the two angles
	rotation = destination_angle - robot_angle
	print("Rotation: " + str(rotation))
	
	return rotation
	
	
	
	
def compute_displacement(robot_head, destination_point):

	# Compute the displacement in terms of pixels
	displacement = np.linalg.norm(destination_point) - np.linalg.norm(robot_head)
	
	# Determine the conversion ration of the displacement value to be in cm
	table_length = 200.0
	image_length = 960.0
	conversion_ratio = round(image_length/table_length, 0)
	
	# Convert the displacement from pixels to cm
	displacement = displacement * conversion_ratio
	print("Displacement: " + str(displacement))
	
	return displacement
		
		



	
	
	
	

