#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import os
import skimage.io
import matplotlib.pyplot as plt
import Segmentation.segmentation_functions as seg
import controller_function as control
import math
from iapr.webcam import WebcamVideoStream
import matplotlib.pyplot as plt
from iapr.robot import Robot
from Digits_classification import *

from Digits_classification.model_cnn_complex import *
import numpy as np 
import matplotlib.pyplot as plt 
import torch
from skimage.transform import resize

from classify_digits import *


# Create a webcam video stream
#   Note: src=0 links to the USB Webcam of the computers provided for the project
wvs = WebcamVideoStream(src=0)

# Read most recent frame
image = wvs.read()

# Directory of the images relative to the current directory
data_base_path = os.path.join(os.pardir, 'data')
data_folder = 'Parcours_updated'
data_path = os.path.join(data_base_path, data_folder)
black_ref = skimage.io.imread(os.path.join(data_path,'black_ref')+'.jpg')

# Get the arrow mask which will later be used to filter out the arrow from the shapes segmentation	
arrow_negative = seg.get_arrow_negative(image, black_ref)

# Normalize the intensities of the images.
image_equalized = seg.intensity_normalization(image)

# Segment the image shapes (excluding the black arrow)
shapes_mask = seg.segment_image(image_equalized, arrow_negative)

shapes_extraction = seg.extract_shapes(image_equalized, shapes_mask)
shapes = shapes_extraction[0]

digits_extraction = seg.extract_numbers(image_equalized, shapes_extraction, shapes_mask)
digits_x_coordinates = digits_extraction[1]
digits_y_coordinates = digits_extraction[2]
digits = digits_extraction[0]

# Determine destination
model = torch.load('model_cnn_complex.pt')

digits_class=obtain_prediction(model, digits, False)
sorted_digits=np.argsort(digits_class)
number_empty=np.sum(digits_class==-1)
destination = [digits_x_coordinates, digits_y_coordinates]				

# Extract just the arrow on its own.
arrow = seg.extract_arrow(image, black_ref)
arrow_image = arrow[0]						# Extract the arrow binary image
arrow_head_coordinates = arrow[1]			# Extract the arrow head coordinates
arrow_tail_coordinates = arrow[2]			# Extract the arrow tail coordinates
	
# Rotate the robot
angle = control.compute_rotation(arrow_head_coordinates, compute_displacement, destination)
robot.steer_in_place(angle)

# We arrived at the destination
robot.beep(count=2)

