
import torch
import torch.nn as nn


class CNN_simple(nn.Module):
    def __init__(self, nb_hidden=200):
        super(CNN_simple, self).__init__()
        #FEATURE EXTRACTION
        # Convolutional layer 1: Conv from 1 to 32 channels with a kernel of size 5 + ReLU and max pool of size 3
        self.c1 = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=3, stride=3),
        )
        # Convolutional layer 1: Conv from 32 to 64 channels with a kernel of size 5 + ReLU and max pool of size 2
        self.c2 = nn.Sequential(
            nn.Conv2d(32, 64, kernel_size=5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        # CLASSIFICATION
        
        # Fully connected 1: output of cnn flattened to nb_hidden, normally =200 + ReLU
        self.fc1 = nn.Sequential(
            nn.Linear(256, nb_hidden),
            nn.ReLU()
        )
        
        # Fully connected 2
        self.fc3 = nn.Sequential(
            nn.Linear(nb_hidden, nb_hidden),
            nn.ReLU()
        )

        # Fully connected 3: mapping to output
        self.fc2= nn.Linear(nb_hidden, 10)
           
    def forward(self, x):
        ''' Forward pass of backprop'''
        
        #Feature extraction
        out = self.c1(x)
        out = self.c2(out)
        #Classification flattening output
        out = self.fc1(out.view(x.shape[0],-1))
        out = self.fc3(out)
        out = self.fc2(out)

        return out