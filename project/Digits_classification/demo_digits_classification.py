
from model_cnn_complex import *
import numpy as np 
import matplotlib.pyplot as plt 
import torch
from skimage.transform import resize

from classify_digits import *
import pickle

import random
from scipy.misc import imrotate

# Load input
digits=pickle.load(open('numbers.pkl','rb'))

max_angle = 180

digits_rot = [imrotate(x.astype(float), random.uniform(-max_angle,max_angle),'nearest') for x in digits]


# Load model
model = torch.load('model_cnn_complex.pt')

print(obtain_prediction(model, digits_rot, True))