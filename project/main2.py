# -*- coding: utf-8 -*-

import numpy as np
import os
import skimage.io
import matplotlib.pyplot as plt
import math
import torch
from skimage.transform import resize


import Segmentation.segmentation_functions as seg
from Digits_classification import *
from Digits_classification.model_cnn_complex import *
from Digits_classification.classify_digits import *
import matching_functions as m



# Directory of the images relative to the current directory
data_base_path = os.path.join('data')
data_folder = 'Parcours_updated'
data_path = os.path.join(data_base_path, data_folder)
black_ref = skimage.io.imread(os.path.join(data_path,'black_ref')+'.jpg')
image = skimage.io.imread(os.path.join(data_path,'22')+'.jpg')



# Normalize the intensities of the images.
image_equalized = seg.intensity_normalization(image)

brown_mask = seg.remove_brown_background(image_equalized)

# Get the arrow mask which will later be used to filter out the arrow from the shapes segmentation	
arrow_mask = seg.get_arrow_negative(image, black_ref, brown_mask)

# Segment the image shapes (excluding the black arrow)
shapes_mask = seg.segment_image(image_equalized, arrow_mask, brown_mask)

#plt.imshow(shapes_mask)
#plt.show()

# Extract the shapes, and each shape's corresponding coordinates
shapes_extraction = seg.extract_shapes(image_equalized, shapes_mask)
shapes = shapes_extraction[0]
shapes_y_coordinates = shapes_extraction[1]
shapes_x_coordinates = shapes_extraction[2]

# Extract the digits, and each digits' corresponding coordinates
digits_extraction = seg.extract_numbers(image, shapes_extraction, shapes_mask)
digits_y_coordinates = digits_extraction[1]
digits_x_coordinates = digits_extraction[2]
digits = digits_extraction[0]

# Introduce a circle to the shapes structure. The image matching of this will be the home shape.
circle_img_size = segments[0].shape
circle_img = np.zeros(circle_img_size)
np.sqrt(np.mean(sums)/np.pi)
circle_radius = np.sqrt(np.mean(sums)/np.pi)
circle_center = circle_img_size[0]//2
for i in range(circle_img_size[0]):

	for j in range(circle_img_size[1]):
			if (i-circle_center)**2 + (j-circle_center)**2 <= circle_radius**2 :
			circle_img[i,j] = 1
			
# Append the circle to the shapes structure. The last element of shapes_match_map holds the index of shapes corresponding to the home circle
shapes.append(circle_img)

# For every shape, find its match
shape_match_map = m.match_shapes(shapes)

# Determine destination
model = torch.load('Digits_classification/model_cnn_complex.pt')
digits_class=obtain_prediction(model, digits, False)

# pair shapes
image_pairs = classify_images(shapes, np.array(digits_class))
print(image_pairs)




sorted_digits=np.argsort(digits_class)
skip_unnumbered=np.sum(np.array(digits_class)==-1)

print(sorted_digits[skip_unnumbered])
print(sorted_digits)
print(digits_class[sorted_digits[skip_unnumbered]])
print(digits_class)
	
	
	
# Begin the course
working = True
angle_threshold = 20
distance_threshold = 100

	
# Rotate the robot
while working:

	# Read most recent frame
	image = wvs.read()
	
	# Get the arrow mask which will later be used to filter out the arrow from the shapes segmentation	
	arrow_mask = seg.get_arrow_negative(image, black_ref, brown_mask)

	# Extract just the arrow on its own.
	arrow = seg.extract_arrow(arrow_mask)
	#	seg.snapshot(arrow[0], True)
	arrow_image = arrow[0]						# Extract the arrow binary image
	arrow_head_coordinates = arrow[1]			# Extract the arrow head coordinates
	arrow_tail_coordinates = arrow[2]			# Extract the arrow tail coordinates

	#  Command robot
	angle = control.compute_rotation(image_equalized, arrow_head_coordinates, arrow_tail_coordinates, destination)
	distance = control.compute_displacement(arrow_head_coordinates, destination)

	# print('Distance: {}'.format(distance))
	if abs(angle) > angle_threshold:
		robot.steer_in_place(angle)

	elif (abs(angle) <= angle_threshold) and (distance > distance_threshold):
		robot.move_forward(10)

	else: 
		working = False
		



	# robot.beep(count=1)
# working=True
# destination = destination2
# robot.beep(count=2)
# while working:

	# # Read most recent frame
	# image = wvs.read()
	
	# # Get the arrow mask which will later be used to filter out the arrow from the shapes segmentation	
	# arrow_mask = seg.get_arrow_negative(image, black_ref, brown_mask)

	# # Extract just the arrow on its own.
	# arrow = seg.extract_arrow(arrow_mask)
# #	seg.snapshot(arrow[0], True)
	# arrow_image = arrow[0]						# Extract the arrow binary image
	# arrow_head_coordinates = arrow[1]			# Extract the arrow head coordinates
	# arrow_tail_coordinates = arrow[2]			# Extract the arrow tail coordinates

	# #  Command robot
	# angle = control.compute_rotation(image_equalized, arrow_head_coordinates, arrow_tail_coordinates, destination)
	# distance = control.compute_displacement(arrow_head_coordinates, destination)

	# print('Distance: {}'.format(distance))
	# if abs(angle) > angle_threshold:
		# robot.steer_in_place(angle)

	# elif (abs(angle) <= angle_threshold) and (distance > distance_threshold):
		# robot.move_forward(10)

	# else: 
		# working = False
		



	# robot.beep(count=1)
	
# # We arrived at the destination
# robot.beep(count=2)


number_empty=np.sum(np.array(digits_class)==-1)
for i in range(number_empty, len(digits_class)):
    destination = [digits_x_coordinates[sorted_digits[i]], digits_y_coordinates[sorted_digits[i]]]
    seg.snapshot(digits[sorted_digits[i]], True)

    # while working:

	    # # Read most recent frame
	    # image = wvs.read()
	
	    # # Get the arrow mask which will later be used to filter out the arrow from the shapes segmentation	
	    # arrow_mask = seg.get_arrow_negative(image, black_ref, brown_mask)

	    # # Extract just the arrow on its own.
	    # arrow = seg.extract_arrow(arrow_mask)
    # #	seg.snapshot(arrow[0], True)
	    # arrow_image = arrow[0]						# Extract the arrow binary image
	    # arrow_head_coordinates = arrow[1]			# Extract the arrow head coordinates
	    # arrow_tail_coordinates = arrow[2]			# Extract the arrow tail coordinates

	    # #  Command robot
	    # angle = control.compute_rotation(image_equalized, arrow_head_coordinates, arrow_tail_coordinates, destination)
	    # distance = control.compute_displacement(arrow_head_coordinates, destination)

	    # print('Distance: {}'.format(distance))
	    # if abs(angle) > angle_threshold:
		    # robot.steer_in_place(angle)
            # pass

	    # elif (abs(angle) <= angle_threshold) and (distance > distance_threshold):
		    # robot.move_forward(10)
            

	    # else: 
		    # working = False
		



	    # robot.beep(count=1)
    # working=True
    # robot.beep(count=2)
    destination = image_pairs[sorted_digits[i]]
    seg.snapshot(shapes[sorted_digits[i]], True)
    plt.title('Shape with number')
    
    seg.snapshot(shapes[image_pairs[destination]], True)
    plt.title('Shape without number')

    # while working:

	    # # Read most recent frame
	    # image = wvs.read()
	
	    # # Get the arrow mask which will later be used to filter out the arrow from the shapes segmentation	
	    # arrow_mask = seg.get_arrow_negative(image, black_ref, brown_mask)

	    # # Extract just the arrow on its own.
	    # arrow = seg.extract_arrow(arrow_mask)
    # #	seg.snapshot(arrow[0], True)
	    # arrow_image = arrow[0]						# Extract the arrow binary image
	    # arrow_head_coordinates = arrow[1]			# Extract the arrow head coordinates
	    # arrow_tail_coordinates = arrow[2]			# Extract the arrow tail coordinates

	    # #  Command robot
	    # angle = control.compute_rotation(image_equalized, arrow_head_coordinates, arrow_tail_coordinates, destination)
	    # distance = control.compute_displacement(arrow_head_coordinates, destination)

	    # print('Distance: {}'.format(distance))
	    # if abs(angle) > angle_threshold:
		    # robot.steer_in_place(angle)

	    # elif (abs(angle) <= angle_threshold) and (distance > distance_threshold):
		    # robot.move_forward(10)

	    # else: 
		    # working = False

          
home = image_pairs[-1]
seg.snapshot(shapes[home], True)
plt.title('Home')
