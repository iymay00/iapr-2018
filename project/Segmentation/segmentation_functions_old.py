#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import os
import skimage.io
import matplotlib.pyplot as plt
from skimage import exposure
from skimage.feature import canny
from scipy import ndimage as ndi
from skimage.morphology import disk
from skimage.feature import blob_log
from skimage.morphology import square
from matplotlib.patches import Circle, Wedge, Polygon
from skimage.filters import sobel
from skimage.morphology import watershed

def snapshot(image, snapshot_enabled):
	if snapshot_enabled:
		fig, ax = plt.subplots(1, 1, figsize=(6, 6))
		ax.imshow(image, cmap='gray')
		ax.axis('off')

# A function to perform intensity normalization on an image.
def intensity_normalization(image):
    

	# Intensity equalization is done for each channel in the images.
	image_equalized = np.empty(image.shape)

	# Specify the upper and lower percentile levels and clip limit
	p_up = 100	# Higher value means more dark allowed in the final image
	p_low = 10	# Lower value means more light allowed in the final image
	clip_lim = 0.05
	k_size = image.shape[0]/6.0

	# Contrast stretching.
	p_lower_ch1, p_upper_ch1 = np.percentile(image[:,:,0], (p_low, p_up))
	p_lower_ch2, p_upper_ch2 = np.percentile(image[:,:,1], (p_low, p_up))
	p_lower_ch3, p_upper_ch3 = np.percentile(image[:,:,2], (p_low, p_up))
    
	# Rescale the image intensity for each channel
	img_rescale_channel_1 = exposure.rescale_intensity(image[:,:,0], in_range=(p_lower_ch1, p_upper_ch1))
	img_rescale_channel_2 = exposure.rescale_intensity(image[:,:,1], in_range=(p_lower_ch2, p_upper_ch2))
	img_rescale_channel_3 = exposure.rescale_intensity(image[:,:,2], in_range=(p_lower_ch3, p_upper_ch3))
    
	# Equalize the histograms
	channel_1_eqd = skimage.exposure.equalize_adapthist(img_rescale_channel_1, clip_limit = clip_lim, kernel_size=k_size)
	channel_2_eqd = skimage.exposure.equalize_adapthist(img_rescale_channel_2, clip_limit = clip_lim, kernel_size=k_size)
	channel_3_eqd = skimage.exposure.equalize_adapthist(img_rescale_channel_3, clip_limit = clip_lim, kernel_size=k_size)
    
	# Group the three channels into a single image again.
	image_equalized[:,:,0] = channel_1_eqd
	image_equalized[:,:,1] = channel_2_eqd
	image_equalized[:,:,2] = channel_3_eqd

	return image_equalized

# A function to perform segmentation on an image
def segment_image(image):

	# Convert the original image to grayscale
	image = skimage.color.rgb2gray(image)

	# High pass filter to enhance the contours. This is particularly important for the yellow shapes.
	kernel_HPF = np.array([[1, -2, 1],
					[-2,  6, -2],
					[1, -2, 1]])
					
	image = ndi.convolve(image, kernel_HPF)

   	# Use the Canny edge detector in order to identify the edges of the shapes.
    # Reduce the standard deviation in order to INCREASE the edge detector's sensitivity.
	edges = canny(image, sigma=3)

	# Perform dilation to improve the integrity of the segmented borders.
	shapes_mask = skimage.morphology.dilation(edges, square(2))
	sobel_mask = shapes_mask
	
	# Fill the holes that remain within the image using mathematical morphology.
	shapes_mask = ndi.binary_fill_holes(shapes_mask)

	# Perform dilation to improve the integrity of the segmented borders.
	shapes_mask = skimage.morphology.erosion(shapes_mask, square(10))	
	
	return shapes_mask


# A function to extract the image's shapes
def extract_shapes(image, shapes_mask):
	
	plot_blobs = False
	plot_crops = False
	cropped_shapes = []
	final_shapes = []
	
	# Count the number of blobs in the shapes mask
	# We raise the minimum standard deviation for the Gaussian Kernel in order to avoid overcounting blobs.
	blobs = blob_log(shapes_mask, min_sigma=50, max_sigma=50, num_sigma = 1, overlap=0.5)
        	
	# Get all the coordinates of the shapes to extract.
	x_coordinates = [blob[1] for blob in blobs]
	y_coordinates = [blob[0] for blob in blobs]
	radii = [blob[2] for blob in blobs]

	if plot_blobs:
		fig, ax = plt.subplots(1, 1, figsize=(6, 6))
		ax.imshow(shapes_mask)
		ax.axis('off')
		plt.scatter(x_coordinates, y_coordinates)

	for x, y, r in zip(x_coordinates, y_coordinates, radii):
		
		# Compute the dimensions of a square surrounding each image
		r = 4 * r

		if plot_blobs:
			crop = plt.Rectangle((x - (r/2), y - (r/2)), r, r, color='r', fill = False)
			ax.add_artist(crop)
		
		# Compute the boundaries of the crop
		y_upper_crop= y - (r/2)
		y_lower_crop= image.shape[0] - (y + (r/2))
		x_left_crop= x - (r/2)
		x_right_crop= image.shape[1] - (x + (r/2))

		# Make sure we haven't exceeded the boundaries of the image itself		
		if y_upper_crop < 0:
			y_upper_crop = 0
		if y_lower_crop < 0:
			y_lower_crop = 0 # image.shape[0]
		if x_left_crop < 0:
			x_left_crop = 0
		if x_right_crop < 0:
			x_right_crop = 0 # image.shape[1]
		
		# Crop out the shape
		cropped_image = skimage.util.crop(shapes_mask, ((y_upper_crop, y_lower_crop), (x_left_crop,  x_right_crop)) , copy=True)
		cropped_shapes.append(cropped_image)

	# Search for the maximum possible axis length throughout all the crops.
	max_axis = 0
	for c in range(0, len(cropped_shapes) - 1):
		max_axis = max([len(cropped_shapes[c][:,1]), len(cropped_shapes[c][1,:]), max_axis])
	
	# Make sure all the images are the same size. 
	for c in range(0, len(cropped_shapes)):
		
		# If any one of the two image axes are less than the max, pad that axis. 
		final_shape = cropped_shapes[c]

		if len(final_shape[:,1]) < max_axis:
			final_shape = skimage.util.pad(final_shape, ((max_axis - len(final_shape[:,1]), 0), (0,0)), mode = "constant")

		if len(final_shape[1,:]) < max_axis:
			final_shape = skimage.util.pad(final_shape, ((0, 0), (max_axis - len(final_shape[1,:]),0)), mode = "constant")

		# Save the final shape
		final_shapes.append(final_shape)

	# Print the cropped and padded shapes.
	if plot_crops:
		for c in range(0, len(final_shapes)):
	
			fig, ax = plt.subplots(1, 1, figsize=(6, 6))
			ax.imshow(final_shapes[c], cmap='gray')
			ax.axis('off')
			ax.set_title("A Crop of Dimensions: " + str(len(final_shapes[c][:,1])) + " x " + str(len(final_shapes[c][1,:])))


	plt.show()
	
	# Return a list containing the cropped shapes, and the coordinates of each shape's center in the original image.
	return [final_shapes, x_coordinates, y_coordinates, radii]


# A function to extract the image's numbers
def extract_numbers(image, shape_extraction, shapes_mask):

	plot_crops = False
	plot_hist = False
	snapshot_enabled = False
	cropped_shapes = []
	final_shapes = []

	# Convert the original image to grayscale
	image = skimage.color.rgb2gray(image)	
	
	# Create a 2D array of the same dimensions as the image, which will store the markers used in the watershed segmentation.
	markers = np.zeros_like(image)
	
	# Specify the upper and lower percentile levels and clip limit
	p_up = 100	# Increasing this makes high values saturate to "super bright" more easily
	p_low = 90	# Increasing this makes low values saturate to "pitch black" more easily
	clip_lim = 1	# Maximize the contrast
	k_size = image.shape[0]/12.0

	# Equalize the histograms
	image = skimage.exposure.equalize_adapthist(image, clip_limit = clip_lim, kernel_size=k_size)
	
	# Contrast stretching.
	p_lower_ch1, p_upper_ch1 = np.percentile(image, (p_low, p_up))
	image = exposure.rescale_intensity(image, in_range=(p_lower_ch1, p_upper_ch1))

	# Set a threshold. Find the threshold as a fraction of the max intensity in the image
	mx = 0
	for i in range(0,image.shape[0]):
		for j in range(0,image.shape[1]):
			if image[i][j] > mx:
				mx = image[i][j]
				
	threshold = 0.4*mx
	
	image = skimage.morphology.closing(image, square(3))

	# Plot a histogram
	if plot_hist:
		fig, ax = plt.subplots(1, 1, figsize = (6,6))
		ax.hist(image.ravel(), bins= 150)
		ax.axvline(threshold, color = 'r')

   	# Use the Canny edge detector in order to identify the edges of the shapes.
    # Reduce the standard deviation in order to INCREASE the edge detector's sensitivity.
	digits_mask = canny(image, sigma=5)
	digits_mask = skimage.morphology.binary_dilation(digits_mask, selem=square(5))
	digits_mask = ndi.binary_fill_holes(digits_mask)
	digits_mask = skimage.morphology.binary_erosion(digits_mask, selem=disk(5))
	
	# Median filer away any remaining noise
	digits_mask = skimage.filters.median(digits_mask, selem = square(13))
	
	contours_image = image > threshold
	snapshot(contours_image, snapshot_enabled)
	
	# Use region growing by extracting markers to obtain the numbers precisely
	buffer = 0.2*threshold
	markers[contours_image < threshold - buffer ] = 1
	markers[(image * shapes_mask * digits_mask) > 0] = 2
	
	# Generate an elevation map of the brain image using the sobel filter.
	elevation_map = sobel(contours_image)#, digits_mask) 
	snapshot(elevation_map,snapshot_enabled)
	
	# Perform the segmentation on the elevation map, using the computed markers as starting points for the watershed algorithm.
	image = watershed(elevation_map, markers) - 1 # subtract 1 since the outputs are 1 & 2 for foreground and background
	
	# Fill any small gaps that remain using binary closing
	image = skimage.morphology.binary_dilation(image, selem=square(3))
	image = skimage.morphology.binary_erosion(image, selem=square(2))
	
	snapshot(image,snapshot_enabled)
	
	# Extract the shapes as defined by the shape extraction parameters	
	x_coordinates = shape_extraction[1]
	y_coordinates = shape_extraction[2]
	radii = shape_extraction[3]
	
	for x, y, r in zip(x_coordinates, y_coordinates, radii):
		
		# Compte the dimensions of a square surrounding each image
		r = 4 * r
		
		# Compute the boundaries of the crop
		y_upper_crop= y - (r/2)
		y_lower_crop= image.shape[0] - (y + (r/2))
		x_left_crop= x - (r/2)
		x_right_crop= image.shape[1] - (x + (r/2))

		# Make sure we haven't exceeded the boundaries of the image itself		
		if y_upper_crop < 0:
			y_upper_crop = 0
		if y_lower_crop < 0:
			y_lower_crop = 0 
		if x_left_crop < 0:
			x_left_crop = 0
		if x_right_crop < 0:
			x_right_crop = 0
		
		# Crop out the shape
		cropped_image = skimage.util.crop(image, ((y_upper_crop, y_lower_crop), (x_left_crop,  x_right_crop)) , copy=True)
		cropped_shapes.append(cropped_image)

	# Search for the maximum possible axis length throughout all the crops.
	max_axis = 0
	for c in range(0, len(cropped_shapes) - 1):
		max_axis = max([len(cropped_shapes[c][:,1]), len(cropped_shapes[c][1,:]), max_axis])
	
	# Make sure all the images are the same size. 
	for c in range(0, len(cropped_shapes)):
		
		# If any one of the two image axes are less than the max, pad that axis. 
		final_shape = cropped_shapes[c]

		if len(final_shape[:,1]) < max_axis:
			final_shape = skimage.util.pad(final_shape, ((max_axis - len(final_shape[:,1]), 0), (0, 0)), mode = "constant")

		if len(final_shape[1,:]) < max_axis:
			final_shape = skimage.util.pad(final_shape, ((0, 0), (max_axis - len(final_shape[1,:]), 0)), mode = "constant")

		# Save the final shape
		final_shapes.append(final_shape)

	# Print the cropped and padded shapes.
	if plot_crops:
		for c in range(0, len(final_shapes)):
	
			fig, ax = plt.subplots(1, 1, figsize=(6, 6))
			ax.imshow(final_shapes[c], cmap='gray')
			ax.axis('off')
			ax.set_title("A Crop of Dimensions: " + str(len(final_shapes[c][:,1])) + " x " + str(len(final_shapes[c][1,:])))
	
	plt.show()	
	
	# Return a list containing the cropped shapes, and the coordinates of each shape's center in the original image.
	return [final_shapes, x_coordinates, y_coordinates, radii]


